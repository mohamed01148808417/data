<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('password');
            $table->string('image')->nullable();
            $table->boolean('is_admin')->default(0);
            $table->string('fcm_token_android')->nullable();
            $table->string('fcm_token_ios')->nullable();
            $table->integer('confirmation_code')->nullable();
            $table->integer('verification_code')->nullable();
            $table->integer('is_verified')->nullable()->default(0);
            $table->string('address')->nullable();//for providers Only
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
