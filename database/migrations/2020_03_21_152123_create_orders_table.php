<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('order_type',['contract','consultation','special']);
            $table->boolean('has_files')->nullable()->default(0);
            $table->unsignedInteger('user_id');
            $table->enum('pay',['cash', 'paypal','file'])->nullable();

            $table->string('file_payed')->nullable();

            $table->string('execution_time')->nullable();

            $table->string('amount')->nullable();

            $table->enum('status',['new',
                'review',
                'payed',
                'canceled',
                'finished']);

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
