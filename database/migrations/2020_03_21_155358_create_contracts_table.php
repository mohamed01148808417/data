<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->text('details')->nullable();
            $table->text('contract_time')->nullable();
            $table->string('p_one_name')->nullable();
            $table->string('p_one_national')->nullable();
            $table->string('p_one_card')->nullable();
            $table->string('p_one_city')->nullable();
            $table->string('p_one_region')->nullable();
            $table->text('p_one_address')->nullable();
            $table->string('p_one_email')->nullable();
            $table->string('p_one_phone')->nullable();
            $table->string('p_two_name')->nullable();
            $table->string('p_two_national')->nullable();
            $table->string('p_two_card')->nullable();
            $table->string('p_two_city')->nullable();
            $table->string('p_two_region')->nullable();
            $table->text('p_two_address')->nullable();
            $table->string('p_two_email')->nullable();
            $table->string('p_two_phone')->nullable();
            $table->text('special_terms')->nullable();
            $table->text('partial_terms')->nullable();
            $table->text('other_terms')->nullable();
            $table->text('termination_of_contract')->nullable(); // اليه انتهاء العقد
            $table->text('contract_of_law')->nullable(); //  محكمه العقد
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
