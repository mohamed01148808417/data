@extends('layouts.app')

@section('content')


    @push('style')
        <link href="{{ asset('/website/')}}/amssoftech.css" rel="stylesheet" />
{{--        <link href="{{ asset('/website/')}}/bootstrap-3-3-4-dist/post.css" rel="stylesheet" />--}}
         <style>
             #regForm {
                 background-color: #ffffff;
                 padding: 10px;
                 width: 100%;
                 float: left;
                 direction: rtl;
             }
             button {
                 font-family: 'Janna LT' !important;
                 background-color: #98c64c;
                 color: #ffffff;
                 border: none;
                 padding: 10px 20px;
                 font-size: 17px;
                 /*font-family: Raleway;*/
                 cursor: pointer;
             }

             button:hover {
                 opacity: 0.8;
             }

             #prevBtn {
                 background-color: #bbbbbb;
             }

             #nextBtn {
                 float: left;
                 color: #ffffff;
                 background: #176083;
             }

             #submitBtn{
                 float:left;
             }

             input, select {
                 font-family: 'Janna LT' !important;
                 padding: 6px 10px;
                 width: 100%;
                 font-size: 17px;
                 border: 1px solid #aaaaaa;
                 line-height: 25px !important;
             }

             input:hover, input:focus, select:hover, select:focus {
                 outline: 0;
                 border: 1px solid #176083;
             }

             /* Mark input boxes that gets an error on validation: */
             input.invalid {
                 background-color: #ffdddd;
             }

             #snackbar {
                visibility: hidden;
                min-width: 250px;
                margin-left: -125px;
                background-color: #ef003b;
                color: #fff;
                text-align: center;
                border-radius: 2px;
                padding: 16px;
                position: fixed;
                z-index: 1;
                left: 50%;
                top: 30px;
                font-size: 17px;
            }

            #snackbar.show {
                visibility: visible;
                -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
                animation: fadein 0.5s, fadeout 0.5s 2.5s;
            }

            @-webkit-keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @-webkit-keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

            @keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

             .login-bg a:hover, .login-bg a:focus{
                 color: #555;
                 outline: none;
             }
             .login-link a{
                 border-radius: 25px;
                 border: 2px solid #999;
                 outline: none;
             }
             .login-link a i{
                 background-color: #0092dc;
                 border-bottom-right-radius: 20px;
                 border-top-right-radius: 20px;
             }
             .login-logo{
                 padding: 0;
             }
        </style>

    @endpush

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-8">
                <div class="text-center">
                    <a href="/"><img src="{{asset('/sheari-logo-1.png')}}" class="login-logo" /></a>
                </div>
                <div class="profile-bar">
                    <form id="regForm" action="#!">
                        <h1><span>سجل معنا</span></h1>
                        <div class="tab">
                            <div class="time-bg">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="login-link">
                                            <a href="{{route('clientRegister')}}"><i class="fa fa-user"></i>  كعضو (مستفيد)</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="login-link">
                                            <a href="{{route('providerRegister')}}"><i class="fa fa-briefcase"></i>  كمقدم خدمة</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="login-bg" style="background-color: #999">
                    <h5><span style="background-color: #999; color: #fff">عضو في شعاري؟</span></h5>
{{--                    <a href="/en/guest/signup"><i class="fa fa-language"></i> English</a>--}}
                    <a style="background-color:#fbfbfb; border-radius: 21px; padding: 7px 41px; border: 1px solid #fbfbfb;" href="/login">الدخول</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://sheari.com.sa/common/User/ar/bootstrap-3.3.4-dist/js/Jq.js"></script>
{{--    <script src="https://sheari.com.sa/common/User/ar/bootstrap-3.3.4-dist/post.js"></script>--}}
    <script>
        $(document).ready(function() {
        });
    </script>
@endpush

