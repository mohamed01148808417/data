<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> Dashboard </title>

    <!-- Scripts -->

    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <!-- Fonts -->
    <link href="{{ asset('/website/')}}/bootstrap-3-3-4-dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.min.css" rel="stylesheet" />
    <link href="{{ asset('/website/')}}/ams.css" rel="stylesheet" />
    <link rel="shortcut icon" type="image/x-icon"
          href="{{ asset('sheari-logo-1.png')}}"/>



    @stack('style')
<STYLE>
    .invalid-feedback{
        color: red;
    }
    /* ==========================================
    * CUSTOM UTIL CLASSES
    * ==========================================
    *
    */

    .progress {
        width: 150px;
        height: 150px;
        background: none;
        position: relative;
    }

    .progress::after {
        content: "";
        width: 100%;
        height: 100%;
        border-radius: 50%;
        border: 6px solid #eee;
        position: absolute;
        top: 0;
        left: 0;
    }

    .progress>span {
        width: 50%;
        height: 100%;
        overflow: hidden;
        position: absolute;
        top: 0;
        z-index: 1;
    }

    .progress .progress-left {
        left: 0;
    }

    .progress .progress-bar {
        width: 100%;
        height: 100%;
        background: none;
        border-width: 6px;
        border-style: solid;
        position: absolute;
        top: 0;
    }

    .progress .progress-left .progress-bar {
        left: 100%;
        border-top-right-radius: 80px;
        border-bottom-right-radius: 80px;
        border-left: 0;
        -webkit-transform-origin: center left;
        transform-origin: center left;
    }

    .progress .progress-right {
        right: 0;
    }

    .progress .progress-right .progress-bar {
        left: -100%;
        border-top-left-radius: 80px;
        border-bottom-left-radius: 80px;
        border-right: 0;
        -webkit-transform-origin: center right;
        transform-origin: center right;
    }

    .progress .progress-value {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1;
    }

    /*
    *
    * ==========================================
    * FOR DEMO PURPOSE
    * ==========================================
    *
    */

    body {

    }
    .progress-bar {
        border-color: #176083 !important;
    }
    .rounded-lg {
        border-radius: 1rem;
    }

    .text-gray {
        color: #aaa;
    }

    div.h4 {
        line-height: 1rem;
    }
    #myFooter {
        background-color: #006bb3;
        color: white;
    }

    #myFooter .row {
        margin-bottom: 60px;
    }

    #myFooter .info{
        text-align: justify;
        color: #afb0b1;
    }

    #myFooter ul {
        list-style-type: none;
        padding: 0;
        line-height: 1.7;
    }
    .form-control{
        padding: 0px 6px;
    }
    #myFooter h5 {
        font-size: 18px;
        color: white;
        font-weight: bold;
        margin-top: 30px;
    }

    #myFooter .logo{
        margin-top: 10px;
        border: none;
        float: none;
    }

    #myFooter .second-bar .logo a{
        color:white;
        font-size: 18px;
         font-weight: bold;
        line-height: 68px;
        margin: 0;
        padding: 0;
    }

    #myFooter a {
        color: #d2d1d1;
        text-decoration: none;
    }

    #myFooter a:hover,
    #myFooter a:focus {
        text-decoration: none;
        color: white;
    }

    #myFooter .second-bar {
        text-align: center;
        background-color: #175172;
        text-align: center;
    }

    #myFooter .second-bar a {
        font-size: 22px;
        color: #9fa3a9;
        padding: 10px;
        transition: 0.2s;
        line-height: 68px;
    }

    #myFooter .second-bar a:hover {
        text-decoration: none;
    }

    #myFooter .social-icons {
        float:right;
    }


    #myFooter .facebook:hover {
        color: #0077e2;
    }

    #myFooter .google:hover {
        color: #ef1a1a;
    }

    #myFooter .twitter:hover {
        color: #00aced;
    }

    @media screen and (max-width: 767px) {
        #myFooter {
            text-align: center;
        }

        #myFooter .info{
            text-align: center;
        }
    }



    /* CSS used for positioning the footers at the bottom of the page. */
    /* You can remove this. */



    .content{
        flex: 1 0 auto;
        -webkit-flex: 1 0 auto;
        min-height: 200px;
    }

    #myFooter{
        flex: 0 0 auto;
        -webkit-flex: 0 0 auto;
    }
</STYLE>

    <!-- Styles -->
 </head>
<body>
    <div id="app">

        <main class="py-4">
            @yield('content')
        </main>

    </div>

{{--    <div class="counter-bg">--}}
{{--        <div class="container">--}}
{{--            <div class="col-xs-12 col-sm-6 col-md-6">&copy; copyright {{date('Y')}}. Sheari. All right reserved.</div>--}}

{{--        </div>--}}
{{--    </div>r--}}


    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link href="{{ asset('/website/')}}/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <script src="{{ asset('/website/')}}/bootstrap-3-3-4-dist/js/Jq.js"></script>
    <script src="{{ asset('/website/')}}/bootstrap-3-3-4-dist/js/bootstrap.min.js"></script>
    <script src="{{ asset('/website/')}}/bootstrap-3-3-4-dist/post.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @include('sweet::alert')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>




    <script>
        $(window).scroll(function () {
            var hT = $('#circle').offset().top,
                hH = $('#circle').outerHeight(),
                wH = $(window).height(),
                wS = $(this).scrollTop();
            console.log((hT - wH), wS);
            if (wS > (hT + hH - wH)) {
                $('.count').each(function () {
                    $(this).prop('Counter', 0).animate({
                        Counter: $(this).text()
                    }, {
                        duration: 2000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                }); {
                    $('.count').removeClass('count').addClass('counted');
                };
            }
        });
    </script>
        <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

    <script>


        $(function() {

            $(".progress").each(function() {

                var value = $(this).attr('data-value');
                var left = $(this).find('.progress-left .progress-bar');
                var right = $(this).find('.progress-right .progress-bar');

                if (value > 0) {
                    if (value <= 50) {
                        right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
                    } else {
                        right.css('transform', 'rotate(180deg)')
                        left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
                    }
                }

            })

            function percentageToDegrees(percentage) {

                return percentage / 100 * 360

            }

        });


    </script>



    @stack('script')
</body>
</html>
