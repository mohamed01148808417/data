@extends('admin.layout')
@section('title')
   الشرةط والاحكام
@endsection

@section('content')
    <!-- Vertical form options -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">التحكم ب الشروط والاحكام</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    {!!Form::open( ['route' => 'updateSettings' ,
                    'class'=>'form phone_validate', 'method' => 'Post','files' => true]) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group col-md-12 pull-left">
                        <label>الشروط والاحكام بالعربي </label>
                        {!! Form::textarea("terms",(isset($setting))?$setting->terms
                        : null,['class'=>'form-control ','placeholder'=>'اكتب الشروط والاحكام هنا '])!!}
                    </div>

                    <br>
                    <br>
                    <div class="text-center col-md-12">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success">حفظ <i class="icon-arrow-left13 position-right"></i></button>
                        </div>
                    </div>

                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="/admin/assets/js/pages/form_layouts.js"></script>
    {!! Html::script('admin/ckeditor/ckeditor.js') !!}
    <script>
        $(document).ready(function () {
            CKEDITOR.replaceClass = 'ck';
        });
    </script>
@endsection