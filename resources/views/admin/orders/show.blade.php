@extends('admin.layout')
@section('title')
     الطلب
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">   طلب من :  {{$order->user->name}} </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row" style="padding: 20px">
                <h1 class="text-center">تفاصيل طلب مراجعه واستشاره</h1>
                <div class="col-md-6" >
                    <small>نوع الطلب </small>
                    <div class="text-center btn btn-default btn-block">{{orderType($order->order_type)}}</div>
                </div>

                <div class="col-md-6 " >
                    <small>حاله الطلب</small>
                    <div class="text-center btn btn-default btn-block">{{orderStatus($order->status)}}</div>
                </div>

                <div class="col-md-6" >
                    <small>نوع الدفع </small>
                    <div class="text-center btn btn-default btn-block">{{orderPayType($order->pay)}}</div>
                </div>

                @if($order->pay == 'file')
                    <div class="col-md-6" >
                        <small>ملف الدفع</small>
                        <div class=""><img style="height: 300px" src="{{getimg($order->file_payed)}}" class="img-responsive"></div>
                    </div>
                @endif

                <div class="clearfix"></div>
                <div class="col-md-6 " >
                    <small>مده تنفيذ الطلب</small>
                    <div class="text-center btn btn-default btn-block">{{$order->execution_time}}</div>
                </div>


                <div class="col-md-6" >
                    <small>المبلغ المطلوب للدفع</small>
                    <div class="text-center btn btn-default btn-block">{{$order->amount}}</div>
                </div>


                <div class="col-md-6">
                    <small>تاريخ انشاء الطلب</small>
                    <div class="text-center btn btn-default btn-block">{{$order->created_at}}</div>
                </div>
            </div>
            <div class="row">
                <h2 class="blockquote text-center">بيانات المرسل للطلب</h2>
                <div class="col-md-6" >
                    <small>اسم المستخدم</small>
                    <div class="text-center btn btn-default btn-block">{{$order->user->name}}</div>
                </div>

                <div class="col-md-6" >
                    <small> جوال المستخدم</small>
                    <div class="text-center btn btn-default btn-block">{{$order->user->phone}}</div>
                </div>
                <div class="col-md-6" >
                    <small>صوره المستخدم</small>
                    <div class="text-center btn btn-default btn-block">
                        <img
                            @if($order->user->image)
                                src="{{$order->user->image}}"
                            @endif
                                src="{{asset('/photos/bDhRvcDUxsqK7ZIjaHSPF33fY6WTcyfx39qTyPlP.png')}}"
                        >
                    </div>
                </div>
            </div>

        @if(isset($consultation) && !empty($consultation))
            <div class="row">
                <h2 class="text-center">تفاصيل الطلب</h2>
                <div class="col-md-12" >
                    <small> التفاصيل</small>
                    <div class="text-center btn btn-default btn-block">{{$order->consultation->details}}</div>
                </div>
            </div>

        @elseif(isset($contract) && !empty($contract))
                <div class="row">
                    <h2 class="text-center"></h2>
                    <div class="col-md-12" >
                        <small> التفاصيل</small>
                        <div class="text-center btn btn-default btn-block">{{$order->contract->details}}</div>
                    </div>

                    <div class="col-md-12" >
                        <small> نبذه مختصره عن مده العقد</small>
                        <div class="text-center btn btn-default btn-block">{{$order->contract->contract_time}}</div>
                    </div>

                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="col-md-6"  style="margin: 40px 0px">
                        <small>بيانات الطرف الاول</small>
                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_one_name}}</div>
                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_one_national}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_one_card}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_one_city}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_one_region}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_one_address}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_one_email}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_one_phone}}</div>
                    </div>

                    <div class="col-md-6" style="margin: 40px 0px" >
                        <small>بيانات الطرف الثاني</small>
                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_two_name}}</div>
                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_two_national}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_two_card}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_two_city}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_two_region}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_two_address}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_two_email}}</div>

                        <div class="text-center btn btn-default btn-block">
                            {{$order->contract->p_two_phone}}</div>
                    </div>


                    <div class="col-md-12" >
                        <small> الشروط الخاصه بالعقد</small>
                        <div class="text-center btn btn-default btn-block">{{$order->contract->special_terms}}</div>
                    </div>


                    <div class="col-md-12" >
                        <small> الشروط الجزئيه بالعقد</small>
                        <div class="text-center btn btn-default btn-block">{{$order->contract->partial_terms}}</div>
                    </div>

                    <div class="col-md-12" >
                        <small> المحكمه المختصه</small>
                        <div class="text-center btn btn-default btn-block">{{$order->contract->contract_of_law}}</div>
                    </div>

                    <div class="col-md-12" >
                        <small> إليه انتهاء العقد</small>
                        <div class="text-center btn btn-default btn-block">{{$order->contract->termination_of_contract}}</div>
                    </div>
                </div>

        @elseif(isset($special) && !empty($special))
                <div class="row">
                    <h2 class="text-center">تفاصيل الطلب</h2>
                    <div class="col-md-12" >
                        <small> التفاصيل</small>
                        <div class="text-center btn btn-default btn-block">{{$order->special->details}}</div>
                    </div>
                </div>
            @else
        @endif


            @if($order->images)
                <div class="row">
                    <h3 class="text-center">صور الطلب</h3>
                    @foreach($order->images as $image)
                        <div class="col-md-3"><img class="img-responsive" src="{{getimg($image->image)}}"></div>
                    @endforeach
                </div>
            @endif

           <div class="row" style="padding: 20px">
                <h1>العمليات علي الطلب </h1>

               <form style="padding-bottom: 40px" class="search-form" method="get" action="{{route('change_status')}}">
               <div class="col-md-8">
                   <input type="hidden" value="{{$order->id}}" name="order_id">
                   <select class="form-control" name="status">
                       <option value="new" {{($order->status == 'new')? "selected": "" }}>جديد</option>
                       <option value="review" {{($order->status == 'review')? "selected": "" }}>تحت المراجعه</option>
                       <option value="payed" {{($order->status == 'payed')? "selected": "" }}>مدفوع</option>
                       <option value="canceled" {{($order->status == 'canceled')? "selected": "" }}>ملغي</option>
                       <option value="finish" {{($order->status == 'finished')? "selected": "" }}>منتهي</option>
                   </select>
               </div>
               <div class="col-md-4">
                   <input type="submit" name=""
                          class="btn btn-block btn-sign" value="تغير الحاله">
               </div>
               </form>

               <h2 class="text-center" style="margin: 40px 0px">ارسال بريد الكتروني للعميل</h2>
               <!-- Button trigger modal -->
               <div class="text-center">
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                       اضغط هنا لكتابه التفاصيل الخاصه بالرساله
                   </button>

               </div>

               <!-- Modal -->
               <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                   <div class="modal-dialog" role="document">
                       <div class="modal-content">
                           <div class="modal-header">
                               <h5 class="modal-title" id="exampleModalLabel"></h5>
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                               </button>
                           </div>
                           <form action="{{route('sending_email')}}" >
                           <div class="modal-body">
                               <div class="form-group">
                                   <label for="address">عنوان الرسالة</label>
                                   <input name="title" placeholder="من فضلك ادخل عنوان الرساله هنا " type="text" class="form-control">
                               </div>

                               <div class="form-group">
                                   <label for="address">محتوي الرسالة</label>
                                   <textarea
                                           class="form-control" name="msg"
                                           placeholder="من فضلك اكتب محتوي الرساله هنا"></textarea>
                               </div>
                               <input type="hidden" value="{{$order->user->email}}" name="user_email">
                               <input type="hidden" value="{{$order->user->name}}" name="user_name">
                           </div>
                           <div class="modal-footer">
                               <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
                               <button type="submit" class="btn btn-primary">ارسال البريد للعميل</button>
                           </div>
                           </form>
                       </div>
                   </div>
               </div>
           </div>
        </div>
    </div>
    <!-- /basic initialization -->
    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا الرساله ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف المدينة تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }
    </script>


@endsection
@section('script')
    <script type="text/javascript" src="{{asset('admin/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/pages/datatables_extension_buttons_init.js')}}"></script>
@endsection
