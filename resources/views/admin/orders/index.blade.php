@extends('admin.layout')
@section('title')
     كافه الطلبات
@endsection

@section('header')

@endsection
@section('content')

    <!-- Basic initialization -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">كل الطلبات</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            عرض كل الطلبات الفرعية والتحكم بهم وبكل العمليات الخاصة بهم مع امكانية البحث وتصدير تقارير وملفات وطباعتهم
        </div>

        <div class="row">
            <div class="col-md-4">
                <a class="btn btn-block btn-warning btn-rounded" href="{{route('orders.index')}}?order_type=contract">ترشيح طلبات صياغة عقد</a>
            </div>
            <div class="col-md-4">
                <a class="btn btn-block btn-success btn-rounded" href="{{route('orders.index')}}?order_type=consultation">ترشيح طلبات المراجعه والاستشاره</a>
            </div>
            <div class="col-md-4">
                <a class="btn btn-block btn-primary btn-rounded" href="{{route('orders.index')}}?order_type=special">ترشيح الطلبات الخاصة</a>
            </div>

        </div>

        <table class="table datatable-button-init-basic">
            <thead>
            <tr>
                <th> # </th>
                <th>الرقم
                </th>
                <th>الاسم</th>
                <th>الايميل</th>
                <td>نوع الطلب</td>
                <td>مده تنفيذ الطلب</td>
                <td>طريقه الدفع</td>
                <td>حاله الطلب</td>
                <th>العمليات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->id}}</td>
                    <td>{{$item->user->name}}</td>
                    <td>{{$item->user->email}}</td>
                    <td>{{orderType($item->order_type)}}</td>
                    <td>{{$item->execution_time}} </td>
                    <td>{{orderPayType($item->pay)}}</td>
                    <td>{{orderStatus($item->status)}}</td>
                    {!!Form::open( ['route' => ['orders.destroy',$item->id] ,'id'=>'delete-form'.$item->id, 'method' => 'Delete']) !!}
                    {!!Form::close() !!}
                    <td>
                        <a href="{{route('orders.show',$item->id)}}"  data-toggle="tooltip" data-original-title="مشاهده"> <i class="icon-eye4 text-inverse text-info" style="margin-left: 10px"></i> </a>

                        <a href="#" onclick="Delete({{$item->id}})" data-toggle="tooltip" data-original-title="حذف"> <i class="icon-trash text-inverse text-danger" style="margin-left: 10px"></i> </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic initialization -->




    <script>
        function Delete(id) {
            var item_id=id;
            console.log(item_id);
            swal({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا الطلب ؟",
                icon: "warning",
                buttons: ["الغاء", "موافق"],
                dangerMode: true,

            }).then(function(isConfirm){
                if(isConfirm){
                    document.getElementById('delete-form'+item_id).submit();
                }
                else{
                    swal("تم االإلفاء", "حذف المدينة تم الغاؤه",'info',{buttons:'موافق'});
                }
            });
        }
    </script>


@endsection
@section('script')
    <script type="text/javascript" src="{{asset('admin/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/assets/js/pages/datatables_extension_buttons_init.js')}}"></script>
@endsection
