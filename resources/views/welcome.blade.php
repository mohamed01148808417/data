@extends('layouts.app')

@section('content')

    <div class="department">
        <div class="container">
            <h1><span>الأقسام الرئيسية</span></h1>
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/img_21.png" alt="" width="300px" height="154px"/>
                        <h4> العروض التقديمية </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/ING_33594_1838291.jpg" alt="" width="300px" height="154px"/>
                        <h4> الواقع الافتراضي VR</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/bigstock-Creative-Business-Idea-5567203711.jpg" alt="" width="300px" height="154px"/>
                        <h4>أفكار دعائية حصرية </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/10x10ft_jpg_640x640.jpg" alt="" width="300px" height="154px"/>
                        <h4>أنظمة الصوت والضوء</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/montage-video2.jpg" alt="" width="300px" height="154px"/>
                        <h4>الإنتاج والإخراج الفني </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/1_917927_large1.jpg" alt="" width="300px" height="154px"/>
                        <h4>البحوث والخدمات الإلكترونية </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/27935_492190677470673_1848486000_n.jpg" alt="" width="300px" height="154px"/>
                        <h4>التحليل والنقد الإعلامي</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/Digital-Marketing-blog-background-blog-3.jpg" alt="" width="300px" height="154px"/>
                        <h4>التسويق الإلكتروني</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/cf224f17cde35362daaa6977bf70579a.jpg" alt="" width="300px" height="154px"/>
                        <h4>التصميم الهندسي </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/p0396l8d.jpg" alt="" width="300px" height="154px"/>
                        <h4>التصوير الرقمي</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/000-9785617501462279837104.JPG" alt="" width="300px" height="154px"/>
                        <h4>التغطيات الإعلامية </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/images3.jpg" alt="" width="300px" height="154px"/>
                        <h4>التمثيل</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/5-3333.jpg" alt="" width="300px" height="154px"/>
                        <h4>الحملات الدعائية والانتخابية</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/Making_of_Cars_3__2017__03.jpg" alt="" width="300px" height="154px"/>
                        <h4>الرسوم الكرتونية المتحركة</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/900x577-1_-59eedd09a165f.jpg" alt="" width="300px" height="154px"/>
                        <h4>الطباعة الرقمية </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/hqdefault.jpg" alt="" width="300px" height="154px"/>
                        <h4>الفن التشكيلي والخط </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/قصص_من_تاريخ_الإسلام.jpg" alt="" width="300px" height="154px"/>
                        <h4>القصاصون والرواة</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/video11.jpg" alt="" width="300px" height="154px"/>
                        <h4>المناهج الإلكترونية</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/multimedia11.png" alt="" width="300px" height="154px"/>
                        <h4>المهارات الإذاعية </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/th_330843-1490655528-microphone_02_vector_1807522.jpg" alt="" width="300px" height="154px"/>
                        <h4>المواهب الفنية والإنشاد</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/business-plan1.jpg" alt="" width="300px" height="154px"/>
                        <h4>تصميم إنفو جرافيك</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/Artist-fida-al-hisan-Design-for-woman.jpg" alt="" width="300px" height="154px"/>
                        <h4>تصميم الأزياء والمجوهرات </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/1edcb4d42d84728d356cee690d190901.jpg" alt="" width="300px" height="154px"/>
                        <h4>تصميم المجسمات</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/images__1_1.jpg" alt="" width="300px" height="154px"/>
                        <h4>تصميم جرافيك - مطبوعات</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/11.jpg" alt="" width="300px" height="154px"/>
                        <h4>تصميم مواقع وتطبيقات </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/images__2_.jpg" alt="" width="300px" height="154px"/>
                        <h4>تصميم وسائل تعليمية </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/Microsoft-Office-20162.png" alt="" width="300px" height="154px"/>
                        <h4>تطبيقات أوفيس</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/10404848_10152266137384263_162106171_o.jpg" alt="" width="300px" height="154px"/>
                        <h4>تقديم دورات جرافيك</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/معلومات-عن-تخصص-تقنية-المعلومات1.jpg" alt="" width="300px" height="154px"/>
                        <h4>تقنيات رقمية حديثة  </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/العرضة-النجدية-3.jpg" alt="" width="300px" height="154px"/>
                        <h4>تنظيم الاحتفالات واللقاءات </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/img_2.png" alt="" width="300px" height="154px"/>
                        <h4>شاشات العرض التفاعلية</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/5b44a7cca102a.jpg" alt="" width="300px" height="154px"/>
                        <h4>فرق العروض الترفيهية</h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/DJ4FGBFXkAIRm2b.jpg" alt="" width="300px" height="154px"/>
                        <h4>هدايا دعائية وإعلانية </h4>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a href="../../index.html">
                        <img src="../../common/Admin/Department/900x504-1_-58263a507ca091.jpg" alt="" width="300px" height="154px"/>
                        <h4>هوية الزي الموحد </h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
