@extends('layouts.user')

@section('content')

    @push('style')

        <style>
            .loader {
                border: 16px solid #f3f3f3;
                border-radius: 50%;
                border-top: 16px solid #3498db;
                width: 120px;
                height: 120px;
                -webkit-animation: spin 2s linear infinite; /* Safari */
                animation: spin 2s linear infinite;
            }

            /* Safari */
            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
            #snackbar {
                visibility: hidden;
                min-width: 250px;
                margin-left: -125px;
                background-color: #ef003b;
                color: #fff;
                text-align: center;
                border-radius: 2px;
                padding: 16px;
                position: fixed;
                z-index: 1;
                left: 50%;
                top: 30px;
                font-size: 17px;
            }

            #snackbar.show {
                visibility: visible;
                -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
                animation: fadein 0.5s, fadeout 0.5s 2.5s;
            }

            @-webkit-keyframes fadein {
                from {
                    top: 0;
                    opacity: 0;
                }
                to {
                    top: 30px;
                    opacity: 1;
                }
            }

            @keyframes fadein {
                from {
                    top: 0;
                    opacity: 0;
                }
                to {
                    top: 30px;
                    opacity: 1;
                }
            }

            @-webkit-keyframes fadeout {
                from {
                    top: 30px;
                    opacity: 1;
                }
                to {
                    top: 0;
                    opacity: 0;
                }
            }

            @keyframes fadeout {
                from {
                    top: 30px;
                    opacity: 1;
                }
                to {
                    top: 0;
                    opacity: 0;
                }
            }
        </style>


    @endpush



    <div class="container">
        <div class="row profile-bg mar-bot">

            <div class="col-xs-12 col-sm-8 col-md-9">
                <div class="profile-bar">
                    <div class="box">
                        <h2 class="text-right"><span style="font-size: 26px !important;">الترقية والدفع</span></h2>
                        <div class="row">
                            <h2 class="text-center" style="color: #222; font-weight: bold; font-size: 20px; margin-bottom: 0;">تفضل بالدخول لاستكمال عمليه الدفع والحصول على الخدمة</h2>
                            <form method="post" class="text-center" action="{{route('user.account_upgrade')}}">
@csrf                 <div class=" text-center loader" id="spinner" style=" display: none;    margin: 0 auto;"></div>

                                <div class="text-center">

                                    <button id="clickpay" type="submit" class="text-center" style="text-align: center;
    background-color: #3a66c6;
    color: white;">

                                        <i class="fa fa-cc-visa"></i> <i
                                            class="fa fa-cc-mastercard"></i> PayPal with Credit / Debit Card, Netbanking
                                    </button>

                                </div>
                            </form>

                        </div>

                    </div>


                </div>
            </div>
            @include('user.side')

        </div>
    </div>

    <!-- Bank Details -->
     <!-- End Bank Details -->

    @push('script')

        <script>
            $("#clickpay").click(function(){
                $("#spinner").show();
                $('#clickpay').hide();
            });
        </script>
    @endpush
    <div id="snackbar">Feedback submitted successfully.</div>
@endsection
