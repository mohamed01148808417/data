@extends('layouts.user')

@section('content')

    @push('style')

        <link href="https://sheari.com.sa/common/User/ar/bootstrap-3.3.4-dist/post.css" rel="stylesheet" />
        <style>
            #snackbar {
                visibility: hidden;
                min-width: 250px;
                margin-left: -125px;
                background-color: #ef003b;
                color: #fff;
                text-align: center;
                border-radius: 2px;
                padding: 16px;
                position: fixed;
                z-index: 1;
                left: 50%;
                top: 30px;
                font-size: 17px;
            }

            #snackbar.show {
                visibility: visible;
                -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
                animation: fadein 0.5s, fadeout 0.5s 2.5s;
            }

            @-webkit-keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @-webkit-keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

            @keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }
        </style>


    @endpush

     <div class="container">
        <div class="row profile-bg mar-bot">
            <div class="col-xs-12 col-sm-8 col-md-9">
                <div class="profile-bar2">
                    <h1><span><i class="fa fa-bell-o"></i> التنبيهات</span>
                    </h1>
                </div>
                <h1><span>
                        <a href="{{route('user.delete_notifications')}}"
                             style="float: left;border-radius: 10px !important; margin: 15px 0px" class="btn btn-warning">مسح التنبيهات</a></span></h1>


                @foreach (\App\Notification::where('user_id',auth()->id())->where('read','no')
               ->latest()->get() as
               $notification)

                <div class=" col-xs-12 alert alert-info"
                     role="alert"
                     style="background:#fff; border: 1px solid #ccc; border-radius: 18px;">
                        <span class="fa fa-info-circle" aria-hidden="true"></span>
                        <span class="sr-only">تنبيه :</span>
                        <a href="#" class="alert-link"><span style="color: #bcc732; font-size: 16px; font-weight: bold"> {{$notification->title}} </span> , رقم  الطلب :  {{$notification->item_id}}</a>
                </div>

                @endforeach


            </div>


            @include('user.side')

        </div>

    </div>
     <div id="snackbar">Feedback submitted successfully. </div>

 @endsection
