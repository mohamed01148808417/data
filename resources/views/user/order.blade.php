@extends('layouts.user')

@section('content')

    @push('style')

        <style>
            #snackbar {
                visibility: hidden;
                min-width: 250px;
                margin-left: -125px;
                background-color: #ef003b;
                color: #fff;
                text-align: center;
                border-radius: 2px;
                padding: 16px;
                position: fixed;
                z-index: 1;
                left: 50%;
                top: 30px;
                font-size: 17px;
            }

            #snackbar.show {
                visibility: visible;
                -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
                animation: fadein 0.5s, fadeout 0.5s 2.5s;
            }

            @-webkit-keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @-webkit-keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

            @keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

            .profile-bg .order-md-1 p{

                box-shadow: inset 2px 0px 4px 0px rgba(0,0,0,.25);
                background-color: #fefefe;
                width: 100%;
                min-height: 35px;
                border-radius: 5px;
                text-align: center;
                line-height: 35px;
                padding: 5px;
                color:
                        #222;
                font-size: 15px;
            }
            .order-md-1 button{
                display: block;
                margin-bottom: 15px;
                width: 200px;
                border-radius: 9px !important;
                text-align: center;
            }
        </style>


    @endpush

    <div class="container">
        <div class="row profile-bg mar-bot">
            <div class="col-xs-12 col-sm-8 col-md-9" style="margin-top: 30px">

                    <div class="row">

                        <div class="col-md-10 order-md-1">
                            <!--<h4 class="mb-3">    رقم  الطلب : {{$order->id}}</h4>-->
                            <br>
                            <form class="needs-validation" novalidate>
                                <div class="mb-3">
                                    <h4 for="username">رقم الطلب</h4>
                                    <p>{{$order->id}}</p>
                                </div>
                                <div class="mb-3">
                                    <label >عنوان الطلب</label>
                                    <p > {{$order->title}}  </p>

                                </div>


                                <div class="mb-3">
                                    <label for="lastName">تفاصيل الطلب</label>
                                    <p>{{$order->details}}</p>
                                </div>

                                <div class="mb-3">
                                    <label for="username">اسم مقدم الطلب</label>
                                     <p>{{$order->user->name}}</p>
                                </div>


                                <div class="mb-3">
                                    <label for="username">بريد مقدم الطلب</label>
                                     <p>{{$order->user->email}}</p>
                                </div>


                                <div class="mb-3">
                                    <label for="username">رقم مقدم الطلب</label>
                                     <p>{{$order->user->phone}}</p>
                                </div>


                                <div class="mb-3">
                                    <label for="time">الأهمية</label>
                                     <p>{{$order->important}}</p>
                                </div>


                                <div class="mb-3">
                                    <label for="time">المدة المتوقعة    </label>
                                     <p>{{$order->expected_time}}</p>
                                </div>


                                <div class="mb-3">
                                    <label for="date">  الميزانية التقديرية </label>
                                     <p>{{$order->expected_money}}</p>
                                </div>


                                @if($order->attachment)
                                <div class="mb-3">
                                    <label for="date">  المرفقات   </label>
                                     <a href="{{getimg($order->attachment)}}">تحميل</a>
                                </div>
                                @endif

                                <div class="row">

                                    @if(auth()->user()->role == 'client')
                                        @if($order->status=='pending')
                                            <a href="#">
                                                <button class="btn btn-primary btn-lg btn-block" type="button"> الطلب جاري </button>
                                            </a>
                                        @endif

                                        @if($order->status=='opening')
                                            <a href="#">
                                                <button class="btn btn-primary btn-lg btn-block" type="button"> الطلب متاح الان </button>
                                            </a>
                                        @endif
                                        @if($order->status=='finished')
                                            <a href="#">
                                                <button class="btn btn-primary btn-lg btn-block" type="button"> الطلب منتهي </button>
                                            </a>
                                        @endif

                                        @if($order->status=='canceled')
                                            <a href="#">
                                                <button class="btn btn-primary btn-lg btn-block" type="button"> تم الغاء طلبك  </button>
                                            </a>
                                        @endif

                                    @endif

                                    @if(auth()->user()->role == 'provider')
                                        @if($order->status=='pending')
                                            <div class="col-md-3 mb-3">
                                                <a href="{{route('user.acceptOrder',[$order->id])}}">
                                                    <button class="btn btn-primary btn-lg btn-block" type="button"> موافقة</button>
                                                </a>
                                            </div>
                                        @elseif($order->status=='finished')
                                                    <div class="col-md-3 mb-3">
                                                        <a href="">
                                                            <button
                                                                    class="btn btn-primary btn-lg btn-block"
                                                                    type="button">الطلب منتهي </button>
                                                        </a>
                                                    </div>
                                        @elseif ($order->status=='canceled')
                                                        <div class="col-md-3 mb-3">
                                                            <a href="#">
                                                                <button class="btn btn-primary btn-lg btn-block"
                                                                        type="button"> الطلب ملغي </button>
                                                            </a>
                                                        </div>

                                        @elseif ($order->status=='opening')
                                                <div class="col-md-3 mb-3">
                                                    <a href="#">
                                                        <button class="btn btn-primary btn-lg btn-block"
                                                                type="button"> تم الموافقه علي الطلب </button>
                                                    </a>

                                                    <div class="col-md-3 mb-3">
                                                        <a href="{{route('user.finishOrder',[$order->id])}}">
                                                            <button class="btn btn-warning btn-lg btn-block" type="button"> انهاء الطلب  </button>
                                                        </a>
                                                    </div>
                                                </div>

                                            @else
                                            <div class="col-md-3 mb-3">
                                                <a href="{{route('user.cancelOrder',[$order->id])}}">

                                                    <button class="btn btn-danger btn-lg btn-block" type="button"> الغاء الطلب</button>
                                                </a>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <a href="{{route('user.finishOrder',[$order->id])}}">
                                                    <button class="btn btn-warning btn-lg btn-block" type="button"> انهاء الطلب  </button>
                                                </a>
                                            </div>
                                        @endif
                                    @endif

                                </div>
                            </form>
                        </div>
                    </div>

            </div>
            @include('user.side')

        </div>
    </div>




 @endsection
