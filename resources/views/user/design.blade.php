@extends('layouts.user')

@section('content')

    @push('style')

        <link href="https://sheari.com.sa/common/User/ar/bootstrap-3.3.4-dist/post.css" rel="stylesheet" />
        <style>
            #snackbar {
                visibility: hidden;
                min-width: 250px;
                margin-left: -125px;
                background-color: #ef003b;
                color: #fff;
                text-align: center;
                border-radius: 2px;
                padding: 16px;
                position: fixed;
                z-index: 1;
                left: 50%;
                top: 30px;
                font-size: 17px;
            }

            #snackbar.show {
                visibility: visible;
                -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
                animation: fadein 0.5s, fadeout 0.5s 2.5s;
            }

            @-webkit-keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @-webkit-keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

            @keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }
        </style>


    @endpush

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 mar-bot">
                <div class="profile-bar">
                    <h1 class="post-head"><span>انتظرونا قريبا</span></h1>

                </div>
            </div>
            @include('user.side')
        </div>
    </div>
    <div id="snackbar">Feedback submitted successfully. </div>

    @push('script')
        <script src="https://sheari.com.sa/common/User/ar/bootstrap-3.3.4-dist/post.js">
        <script>
        function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        }

        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        }
        </script>
        <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
        </script>

        <!-- Load widget code -->
        <script type="text/javascript" src="http://feather.aviary.com/imaging/v2/editor.js"></script>

        <!-- Instantiate the widget -->
        <script type="text/javascript">

            var featherEditor = new Aviary.Feather({
                apiKey: '1234567',
                onSave: function (imageID, newURL) {
                    var img = document.getElementById(imageID);
                    img.src = newURL;
                }
            });

            function launchEditor(id, src) {
                featherEditor.launch({
                    image: id,
                    url: src
                });
                return false;
            }

        </script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <script>
            $(document).ready(function() {
            });
        </script>
    @endpush
@endsection
