@extends('layouts.user')

@section('content')

    @push('style')

        <style>
            #snackbar {
                visibility: hidden;
                min-width: 250px;
                margin-left: -125px;
                background-color: #ef003b;
                color: #fff;
                text-align: center;
                border-radius: 2px;
                padding: 16px;
                position: fixed;
                z-index: 1;
                left: 50%;
                top: 30px;
                font-size: 17px;
            }

            #snackbar.show {
                visibility: visible;
                -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
                animation: fadein 0.5s, fadeout 0.5s 2.5s;
            }

            @-webkit-keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @-webkit-keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

            @keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

            .edit-input small{
                display: inline-block;
                text-align: center;
                width: 100%;
            }
        </style>


    @endpush


    <div class="container">
        <div class="row profile-bg">
            <div class="col-xs-12 col-sm-8 col-md-9">
                <form  action="{{route('user.client')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="profile-bar edit-input mar-bot">
                        <div>
                            <h1><span><i class="fa fa-user"></i> تفاصيل المستخدم </span></h1>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <small>نوع الحساب:</small><br />
                                        <input type="text" value="مزود خدمة " disabled/>
                                    </td>
                                    <td>
                                        <small>اسم المستخدم:</small><br />
                                        <input type="text" value="{{$provider->name}}"  name="name" />
                                    </td>
                                    <td>
                                        <small>البريد الإلكتروني:</small><br />
                                        <input type="email" value="{{$provider->email}}" disabled/>
                                    </td>
                                    <td>
                                        <small>الجوال.:</small><br />
                                        <input type="text" value="{{$provider->phone}}" name="phone"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <small>الاسم:</small><br />
                                        <input type="text" value="{{$provider->name}} " class="width" name="name"/>
                                    </td>
                                    <td colspan="2">
                                        <small>صورة الحساب:</small><br />
                                        <div class="old_image"><img class="img-responsive"><img src="{{getimg($provider->image)}}" /> </div>
                                        <input type="file" class="width" name="image"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <small style="text-align: right"><b>نبذة عامة:</b></small><br />
                                        <textarea name="bio" class="summernote form-control " id="contents" required="required" title="Contents">{{$provider->bio}}</textarea>
                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="2">
                                        <small>
                                            خصم شعاري:
                                        </small>
                                          <input type="text" value="{{$provider->discount}}" class="form-control" name="discount"/>
                                    </td>
                                    <td colspan="2">
                                        <small>
                                            نوع الحساب: Normal                                        <br />
                                        </small><br />
                                        <a href="/user/pay" class="nexr-btn"><b>الارتقاء للعضوية المتميزة</b></a>
                                    </td>
                                </tr>
                            </table>
                        </div>



                        <div class="regi-head">
                            <h2><span><i class="fa fa-briefcase"></i> مجالاتنا</span></h2>
                        </div>

                        <div class="regi-head">
                            <h2><span><i class="fa fa-briefcase"></i> اعمالنا</span></h2>



                            @if($provider->projects)
                                @foreach($provider->projects as $project)

                                    <div class="work_single" style="border: 1px solid #ddd; margin-bottom: 15px">
                                        <h3 style="margin-right: 20px; margin-top: 20px; color: #676767">{{$project->title}}</h3>
                                        <img src="{{getimg($project->file)}}" class="img-responsive" style="height: 200px;margin: 20px; padding:5px; border: 1px solid #ddd; overflow: hidden; width: 80%;">
                                        <br>
                                        <div class="clearfix"></div>
                                        <div class="paragraph" style="margin-right: 20px">{{$project->description}}</div>
                                        <br>

                                        <a href="{{route('user.editProject',['id'=>$project->id])}}" class="nexr-btn" style="margin-right: 20px"><b>تعديل المشروع</b></a>
                                        <a href="{{route('user.removeProject',['id'=>$project->id])}}" class="nexr-btn" style="margin-right: 20px"><b>مسح المشروع</b></a>

                                        <br>
                                        <br>

                                    </div>

                                @endforeach




                            @endif

                            <a href="{{route('user.addProject')}}" class="nexr-btn" style="margin-right: 20px"><b>اضافة مشروع جديد </b></a>
                            <br>
                            <br>
                            <br>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td colspan="2">
                                        <small>الخبرات والمهارات:</small><br />
                                        <b>
                                            <input type="text" value="{{$provider->qualifications}}"
                                                   style="width:100%;" name="skills"/>
                                        </b>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="regi-head">
                            <h2><span><i class="fa fa-map-marker"></i> الموقع</span></h2>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <small>الدولة:</small><br />
                                        <input type="text" value="{{$provider->City->country->name()}}" class="width" disabled/>
                                    </td>
                                    <td>
                                        <small>المدينة:</small><br />
                                        <input type="text" value="{{$provider->City->name()}}" class="width" disabled/>
                                    </td>
                                </tr>

                            </table>
                        </div>


                        <br />
                        <button type="submit" class="nexr-btn"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </form>
            </div>
            @include('user.side')
        </div>
    </div>

    <div id="snackbar">Feedback submitted successfully. </div>

@endsection
