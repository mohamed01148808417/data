@extends('layouts.user')

@section('content')

    @push('style')

        <style>
            #snackbar {
                visibility: hidden;
                min-width: 250px;
                margin-left: -125px;
                background-color: #ef003b;
                color: #fff;
                text-align: center;
                border-radius: 2px;
                padding: 16px;
                position: fixed;
                z-index: 1;
                left: 50%;
                top: 30px;
                font-size: 17px;
            }

            #snackbar.show {
                visibility: visible;
                -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
                animation: fadein 0.5s, fadeout 0.5s 2.5s;
            }

            @-webkit-keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @-webkit-keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

            @keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }
            div.bordered_data small{
                display: block;
                text-align: center;
                margin-bottom: 0px;
                font-weight: bold;
            }
            div.bordered_data b{
                border: 1px solid #ccc;
                display: block;
                border-radius: 14px;
                padding-right: 15px;
                min-height: 25px;
                margin-bottom: 7px;
                font-weight: bold;
                box-shadow: inset 1px 0px 2px 0px
                rgba(0,0,0,.25);
                padding-top: 5px;
                padding-bottom: 1px;
                overflow: hidden;
            }




        </style>


    @endpush

@php
$user=auth()->user();
@endphp
    <div class="container">
        <div class="row profile-bg">
            <div class="col-xs-12 col-sm-8 col-md-9">
                <div class="profile-bar mar-bot">
                    <div>
                        <h1><span><i class="fa fa-user"></i> تفاصيل المستخدم</span>
                            <a href="{{route('user.editMyAccount')}}">
                                <i style="background-color:#7cd92d; padding: 7px 46px; border-radius: 10px; font-style:normal; font-size: 16px" class="badge">تعديل</i></a></h1>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-4 bordered_data">
                                <small>نوع الحساب:</small><br />
                                <b>
                                    @isset($user->provider_type)
                                        @if($user->provider_type=='one')
                                            شخصى
                                        @else
                                            شركة
                                        @endif
                                        @else
                                            شخص
                                                @endisset
                                </b>
                            </div>
                            <div class="col-xs-4 bordered_data">
                                <small>اسم المستخدم:</small><br />
                                <b>{{$user->name}}</b>
                            </div>
                            <div class="col-xs-4 bordered_data">
                                <small>البريد الإلكتروني:</small><br />
                                <b> {{$user->email}}</b>
                            </div>

                            <div class="col-xs-4 bordered_data">
                                <small>الجوال.:</small><br />
                                <b>{{$user->phone}}</b>
                            </div>

                            <div class="col-xs-4 bordered_data">
                                <small>العنوان.:</small><br />
                                <b>{{$user->address}}</b>
                            </div>

                            <div class="col-xs-4 bordered_data">
                                <small>
                                    خصم شعاري:
                                </small><br />

                                <!--                                    <b>0%</b>-->
                                <b>{{$user->discount}}%</b>
                            </div>

                            <div class="col-xs-4 bordered_data">
                                <small>
                                    نوع الحساب:
                                </small><br />
                                @if($user->is_special)
                                    <b>مميز</b>
                                @else
                                    <b>عادي</b>
                                @endif
                            </div>

                            @if($user->emp_no)
                                <div class="col-xs-4 bordered_data">
                                    <small>
                                        عدد الموظفين  :
                                    </small><br />
                                    <b>{{$user->emp_no}}</b>
                                </div>

                            @endif

                            @if($user->creation_year)
                                <div class="col-xs-4 bordered_data">
                                    <small>
                                        سنة الإنشاء    :
                                    </small><br />
                                    <b>{{$user->creation_year}}</b>
                                </div>

                            @endif

                            @if($user->commerical_no)
                                <div class="col-xs-4 bordered_data">
                                    <small>
                                        السجل التجارى    :
                                    </small><br />
                                    <b>{{$user->commerical_no}}</b>
                                </div>

                            @endif
                            <div class="col-xs-4 bordered_data">

                            </div>

                            <div class="col-xs-12 bordered_data">
                                <small style="text-align: right">نبذة عامة:</small><br />
                                <p>
                                    {{$user->bio}}
                                </p>
                            </div>
                        </div>
                    </div>


                    <div class="regi-head">
                        <h2><span><i class="fa fa-briefcase"></i> مجالاتنا</span></h2>
                        <div class="review-bg" id="comment">
                            @foreach(auth()->user()->SubCategories as $item )
                                <a href="{{route('service',$item->id)}}">
                                    <button class="btn btn-primary">{{$item->name()}}</button></a>
                            @endforeach
                        </div>
                    </div>



                    <div class="regi-head">
                        <h2><span><i class="fa fa-briefcase"></i> اعمالنا</span></h2>



                        @if($user->projects)
                            @foreach($user->projects as $project)

                                <div class="work_single" style="border: 1px solid #ddd; margin-bottom: 15px">
                                    <h3 style="margin-right: 20px; margin-top: 20px; color: #676767">{{$project->title}}</h3>
                                    <img src="{{getimg($project->file)}}" class="img-responsive" style="height: 200px;margin: 20px; padding:5px; border: 1px solid #ddd; overflow: hidden; width: 80%;">
                                    <br>

                                    <div class="clearfix"></div>
                                    <div class="paragraph" style="margin-right: 20px">{{$project->description}}</div>
                                    <br>
                                </div>

                            @endforeach

                        @endif

                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">

                            <tr>
                                <td colspan="2">
                                    <small>الخبرات والمهارات:</small><br />
                                    <b>
                                        {{$user->qualifications}}
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">


                                    <div class="checkbox disabled">
                                        <label style="padding-right:20px;">
                                            <input type="checkbox" value="" {{$user->map?"checked":''}} disabled style="margin-right:-20px;">
                                            الموقع علي الخريطة                                        </label>
                                    </div>


                                    <div class="checkbox disabled">
                                        <label style="padding-right:20px;">
                                            <input type="checkbox" value="" {{$user->delivery?"checked":''}} disabled style="margin-right:-20px;">
                                            خدمات التوصيل                                        </label>
                                    </div>


                                    <div class="checkbox disabled">
                                        <label style="padding-right:20px;">
                                            <input type="checkbox" value=""  {{$user->charitable?"checked":''}}  disabled style="margin-right:-20px;">
                                            خصومات خيرية                                        </label>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="regi-head">
                        <h2><span><i class="fa fa-map-marker"></i> الموقع</span></h2>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <small>الدولة:</small><br />
                                    <b>{{$user->City->country->name()}}</b>
                                </td>
                                <td>
                                    <small>المدينة:</small><br />
                                    <b>{{$user->City->name()}}  </b>
                                </td>
                            </tr>
                            
                        </table>
                    </div>

{{--                    <div class="row">--}}
{{--                        <div class="col-xs-12 col-sm-6 col-md-6">--}}
{{--                            <div class="regi-head">--}}
{{--                                <h2><span><i class="fa fa-certificate"></i> السجل التجاري</span></h2>--}}
{{--                            </div>--}}
{{--                            <div class="certi">--}}
{{--                                <img src="/common/User/en/Tradelicense/default.jpg" height="310"/>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-xs-12 col-sm-6 col-md-6">--}}
{{--                            <div class="regi-head">--}}
{{--                                <h2><span><i class="fa fa-youtube"></i> البرومو الدعائي</span></h2>--}}
{{--                            </div>--}}
{{--                            <iframe width="100%" height="310" src="/common/User/en/Tradelicense/default.jpg" frameborder="0" allowfullscreen></iframe>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                </div>
            </div>
            @include('user.side')

        </div>
    </div>


    <div id="snackbar">Feedback submitted successfully. </div>
@endsection
