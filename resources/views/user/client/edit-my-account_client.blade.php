@extends('layouts.user')

@section('content')

    @push('style')

        <style>
            #snackbar {
                visibility: hidden;
                min-width: 250px;
                margin-left: -125px;
                background-color: #ef003b;
                color: #fff;
                text-align: center;
                border-radius: 2px;
                padding: 16px;
                position: fixed;
                z-index: 1;
                left: 50%;
                top: 30px;
                font-size: 17px;
            }

            #snackbar.show {
                visibility: visible;
                -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
                animation: fadein 0.5s, fadeout 0.5s 2.5s;
            }

            @-webkit-keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @-webkit-keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

            @keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }
        </style>


    @endpush


    <div class="container">
        <div class="row profile-bg">
            <div class="col-xs-12 col-sm-8 col-md-9">
                <form  action="{{route('user.client')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="profile-bar edit-input mar-bot">
                        <div>
                            <h1><span><i class="fa fa-user"></i> تفاصيل المستخدم </span></h1>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <small>نوع الحساب:</small><br />
                                        <input type="text" value="مستخدم عادي " disabled/>
                                    </td>
                                    <td>
                                        <small>اسم المستخدم:</small><br />
                                        <input type="text" value="{{$client->name}}"  name="name" />
                                    </td>
                                    <td>
                                        <small>البريد الإلكتروني:</small><br />
                                        <input type="email" value="{{$client->email}}" disabled/>
                                    </td>
                                    <td>
                                        <small>الجوال.:</small><br />
                                        <input type="text" value="{{$client->phone}}" name="phone"/>
                                    </td>


                                </tr>
                                <tr>
                                    <td>
                                        <small>الوظيفة.:</small><br />
                                        <input type="text" value="{{$client->job}}" name="job"/>
                                    </td>
                                    <td colspan="2">
                                        <small>الاسم:</small><br />
                                        <input type="text" value="{{$client->name}} " class="width" name="name"/>
                                    </td>
                                    <td colspan="2">
                                        <small>صورة الحساب:</small><br />
                                        <div class="old_image"><img class="img-responsive"><img src="{{getimg($client->image)}}" /> </div>
                                        <input type="file" class="width" name="image"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <small><b>نبذة عامة:</b></small><br />
                                        <textarea name="bio" class="summernote form-control " id="contents" required="required" title="Contents">{{$client->bio}}</textarea>
                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="">
                                        <small>
                                            نوع الحساب: حساب عادى                                        <br />
                                        </small><br />
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td colspan="2">
                                        <small>الخبرات والمهارات:</small><br />
                                        <b>
                                            <input type="text" value="{{$client->qualifications}}"
                                                   style="width:100%;" name="skills"/>
                                        </b>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="regi-head">
                            <h2><span><i class="fa fa-map-marker"></i> الموقع</span></h2>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <small>الدولة:</small><br />
                                        <input type="text" value="{{$client->City->country->name()}}" class="width" disabled/>
                                    </td>
                                    <td>
                                        <small>المدينة:</small><br />
                                        <input type="text" value="{{$client->City->name()}}" class="width" disabled/>
                                    </td>
                                </tr>

                            </table>
                        </div>


                        <br />
                        <button type="submit" class="nexr-btn"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </form>
            </div>
            @include('user.side')

        </div>
    </div>

    <div id="snackbar">Feedback submitted successfully. </div>

@endsection
