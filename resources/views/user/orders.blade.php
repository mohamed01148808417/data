@extends('layouts.user')

@section('content')

    @push('style')

        <style>
            #snackbar {
                visibility: hidden;
                min-width: 250px;
                margin-left: -125px;
                background-color: #ef003b;
                color: #fff;
                text-align: center;
                border-radius: 2px;
                padding: 16px;
                position: fixed;
                z-index: 1;
                left: 50%;
                top: 30px;
                font-size: 17px;
            }

            #snackbar.show {
                visibility: visible;
                -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
                animation: fadein 0.5s, fadeout 0.5s 2.5s;
            }

            @-webkit-keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @keyframes fadein {
                from {top: 0; opacity: 0;}
                to {top: 30px; opacity: 1;}
            }

            @-webkit-keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }

            @keyframes fadeout {
                from {top: 30px; opacity: 1;}
                to {top: 0; opacity: 0;}
            }
        </style>


    @endpush

    <div class="container">
        <div class="row profile-bg mar-bot">
            <div class="col-xs-12 col-sm-8 col-md-9">
                <div class="profile-bar2">
                    <h1><span><i class="fa fa-question-circle"></i> الطلبات المستمرة</span></h1>
                </div>
                <div class="list-group ">

                    @foreach($pendingProvider as $item)
                        <a href="{{route('user.showOrder',[$item->id])}}" class="list-group-item   col-xs-12 ">
                        <h5 class="label label-info mr-5" style="left: 0px; margin: 6px; position: absolute;">
                            {{OrderStatus($item->status) }}
                        </h5>
                        <h4 class="list-group-item-heading push-right">   <i class="fa fa-feed"></i> {{$item->title}} </h4>
                        <h5 class="list-group-item-heading push-left">    بتوقيت:  {{$item->created_at->format('Y-m-d')}} </h5>
                        <p class="list-group-item-text">
                            {{$item->details}}
                        </p>
                    </a>
                    @endforeach

                    <div class="text-center">
                        {!! $pendingProvider->links() !!}
                    </div>

                </div>


                <div class="profile-bar2">
                    <h1><span><i class="fa fa-question-circle"></i> طلباتى   </span></h1>
                </div>
                <div class="list-group ">

                    @foreach($pendingUser as $item)
                        <a href="{{route('user.showOrder',[$item->id])}}" class="list-group-item   col-xs-12 ">
                            <h5 class="label label-info mr-5" style="left: 0px; margin: 6px; position: absolute;">

                                {{OrderStatus($item->status) }}

                            </h5>
                            <h4 class="list-group-item-heading push-right">   <i class="fa fa-feed"></i> {{$item->title}} </h4>
                            <h5 class="list-group-item-heading push-left">    بتوقيت:  {{$item->created_at->format('Y-m-d')}} </h5>
                            <p class="list-group-item-text">
                                {{$item->details}}
                            </p>
                        </a>
                    @endforeach



                        <div class="text-center">
                            {!! $pendingUser->links() !!}
                        </div>


                </div>

            </div>
            @include('user.side')

        </div>
    </div>




 @endsection
