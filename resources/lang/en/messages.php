<?php
return [
    'wrong_credentials' => 'Sorry, The Data you\'ve Entered is Wrong',
    'success_edit'=>'Your Data Has Been Updated Successfully',
    'success_msg_sent'=>'Your Message Has Been Sent Successfully',
    'not_found'=>'This Request is not found',
    'not_valid_coupon'=>'This Coupon is not valid',
    'success_add_to_cart'=>'Product Has Been Added To Cart Successfully',
    'not_enough'=>'Sorry,Your Cart Should Exceed The minimum charge for this Restaurant',
    'success_sent'=>'Confirmation Code Has Been Sent Successfully to Your Phone and Email',
    'success_code'=>'Your Account Has been Confirmed Successfully',
    'error_code'=>'Sorry , This Code is invalid',
    'wrong_phone'=>'Sorry , This Phone is invalid',
    'success_delete'=>'Item been Deleted Successfully',
    'success_delete_item'=>'Food Had been Deleted Successfully',
    'success_update'=>'Cart Had been Updated Successfully',

];