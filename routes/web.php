<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['prefix' => 'paypal','as'=>'paypal.'], function(){
    Route::get('pay', 'PaypalController@add_to_wallet')->name('pay');
    Route::get('success','PaypalController@onProcessingCourseSuccess')->name('success');
    Route::get('success2','PaypalController@onProcessingCourseSuccess2')->name('success');
    Route::get('canceled','PaypalController@onProcessingCourseCanceled')->name('canceled');
});


Route::get('/', function () {
    return redirect()->route('show_login');
});

Route::get('admin/login','Admin\AdminController@login')->name('show_login');
Route::post('admin/login','Admin\AdminController@doLogin')->name('do_login');
Route::post('admin/logout','Admin\AdminController@logout')->name('do_logout');

Route::group(['prefix' => 'admin','namespace'=>'Admin', 'middleware' => 'admin'], function()
{
    Route::get('dashboard',
        function (){
            return view('admin.main');
        }
    );

    // users

    Route::resource('users', 'UsersController');
    Route::resource('contacts', 'ContactUsController');
    Route::resource('orders', 'OrdersController');

    Route::get('terms/{id}','IndexController@terms')->name('terms');
    Route::post('terms-updated','IndexController@updateSettings')->name('updateSettings');


    Route::get('change_status','OrdersController@changeStatus')->name('change_status');
    Route::get('sending_email','OrdersController@sendingEmail')->name('sending_email');

    // drivers



    // get all users
});
Auth::routes();
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
