<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'paypal','as'=>'paypal.'], function(){
    Route::get('pay', 'PaypalController@add_to_wallet')->name('pay');
    Route::get('success','PaypalController@onProcessingCourseSuccess')->name('success');
    Route::get('success2','PaypalController@onProcessingCourseSuccess2')->name('success');
    Route::get('canceled','PaypalController@onProcessingCourseCanceled')->name('canceled');
});

Route::post('check_email','HomeController@checkEmail');
Route::post('check_phone','HomeController@checkPhone');
Route::get('terms','HomeController@getTerms');
Route::post('contact_us','HomeController@contact');

Route::group(['prefix'=>'auth'],function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('activate', 'AuthController@mobile_activation');
    Route::post('resend_code', 'AuthController@resend_code');
    Route::post('reset', 'AuthController@reset_password');
    Route::post('forget', 'AuthController@forget_password');
    Route::post('check', 'AuthController@check_code');
});

Route::group(['middleware' => ['jwt.auth']], function () {
    Route::post('contact','HomeController@contact');

    Route::group(['prefix' => 'user'], function () {
        Route::post('/upgrade/paypal', 'PaypalController@add_to_wallet');
        //Route::post('/pay', 'PaypalController@onProcessingCourseSuccess');
        Route::get('get_fcm_token','AuthController@getFcmToken');
        Route::post('update_fcm_token','AuthController@updateFcmToken');
        Route::get('logout','AuthController@Logout');
        Route::group(['prefix' => 'update'], function () {
            Route::post('profile', 'AuthController@UpdateProfile');
        });
        Route::get('showNotification/{id}', 'OrderController@showNotification');
        Route::group(['prefix' => 'ordered'], function () {
            Route::get('status/{order_type}','OrderController@orderStatus');
            Route::post('pay','OrderController@orderPay');
            Route::post('add-order', 'OrderController@sendOrder');
        });

    });

});
