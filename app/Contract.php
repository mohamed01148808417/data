<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    //

    protected $fillable = [
        'id',
        'order_id','details','contract_time',
        'p_one_name','p_one_national','p_one_card','p_one_city','p_one_region','p_one_address','p_one_email','p_one_phone',
        'p_two_name','p_two_national','p_two_card','p_two_city','p_two_region','p_two_address','p_two_email','p_two_phone',
        'special_terms','partial_terms',' other_terms',
        'termination_of_contract','contract_of_law','created_at','updated_at'
    ];

    public function order(){
        return $this->belongsTo(Order::class,'order_id','id');
    }

    public function commitments(){
        return $this->hasMany(Commitment::class,'contract_id');
    }
}
