<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact_Us extends Model
{

    protected $fillable = [
        'name', 'email', 'message','user_id'
    ];

    protected $table = 'contact_us';
    public $timestamps = true;
}
