<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderImage extends Model
{
    //

    protected $fillable = ['id','image','order_id'];


    public function order(){
        return $this->belongsTo(Order::class,'order_id');
    }
}
