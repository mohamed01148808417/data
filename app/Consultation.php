<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
    //

    protected $fillable = ['id','order_id','details','created_at','updated_at'];

    public function order(){
        return $this->belongsTo(Order::class,'order_id','id');
    }


}
