<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commitment extends Model
{
    //

    protected $fillable = ['commit','contract_id'];

    public function contract(){
        return $this->belongsTo(Contract::class,'contract_id','id');
    }

}
