<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Special extends Model
{
    //
    protected $fillable = [
        'order_id',
        'details',
        'created_at',
        'updated_at'
    ];

}
