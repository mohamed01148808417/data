<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SingleOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->order_type == 'contract'){

            return [
                'id'=>$this->id,
                'contract'=>$this->contract,
                'commitments'=>$this->contract->commitments,
                'images'=>$this->images,
            ];

        }elseif ($this->order_type == 'consultation'){

            return [
                'id'=>$this->id,
                'consultation'=>$this->consultation,
                'images'=>$this->images
            ];

        }elseif ($this->order_type == 'special'){

            return [
                'id'=>$this->id,
                'special'=>$this->special,
                'images'=>$this->images
            ];

        }


    }
}
