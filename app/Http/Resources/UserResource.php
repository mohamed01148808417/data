<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
            return [
                'id' => $this->id,
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'is_verified' => $this->is_verified,
                'image' => getimg($this->image),
                'token' => $this->token,
                'fcm_token_android' => $this->fcm_token_android,
                'fcm_token_ios'     => $this->fcm_token_ios,
            ];

    }

}
