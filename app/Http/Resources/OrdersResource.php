<?php

namespace App\Http\Resources;

use App\Http\Traits\ApiResponses;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrdersResource extends ResourceCollection
{
    use ApiResponses;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // check order type
        return [
            'orders'=>$this->collection->transform(function ($q){
                if($q->order_type == 'contract'){

                    return [
                        'id'=>$q->id,
                        'order_type'    =>$q->order_type,
                        'payment_method'    =>$q->pay,
                        'execution_time'    =>$q->execution_time,
                        'amount '    =>$q->amount,
                        'status'    => $q->status,
                        'contract' => $q->contract,
                        'commitment' => $q->contract->commitments,
                        'user'=>$q->user,
                        'images'    => $q->images

                    ];

                }elseif ($q->order_type == 'special'){

                    return [
                        'id'=>$q->id,
                        'order_type'    =>$q->order_type,
                        'payment_method'    =>$q->pay,
                        'execution_time'    =>$q->execution_time,
                        'amount '    =>$q->amount,
                        'status'    => $q->status,
                        'special' => $q->special,
                        'user'=>$q->user,
                        'images'    => $q->images

                    ];

                }elseif ($q->order_type == 'consultation'){
                    return [
                        'id'=>$q->id,
                        'order_type'    =>$q->order_type,
                        'payment_method'    =>$q->pay,
                        'execution_time'    =>$q->execution_time,
                        'amount '    =>$q->amount,
                        'status'    => $q->status,
                        'consultation' => $q->consultation,
                        'user'=>$q->user,
                        'images'    => $q->images

                    ];
                }


            })  ,
            'paginate'=>[
//                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' =>  $this->collection->count() != 0 ? ceil($this->count() /$this->collection->count()):1
            ]
        ];
    }
    public function withResponse($request, $response)
    {
        $originalContent = $response->getOriginalContent();
        unset($originalContent['links'],$originalContent['meta']);
        $response->setData($originalContent);
    }
}
