<?php



function printStringResult($apiResult, $arrayMsgs, $printType = 'Alpha')
{
    global $undefinedResult;
    switch ($printType)
    {
        case 'Alpha':
            {
                if(array_key_exists($apiResult, $arrayMsgs))
                    return $arrayMsgs[$apiResult];
                else
                    return $arrayMsgs[0];
            }
            break;

        case 'Balance':
            {
                if(array_key_exists($apiResult, $arrayMsgs))
                    return $arrayMsgs[$apiResult];
                else
                {
                    list($originalAccount, $currentAccount) = explode("/", $apiResult);
                    if(!empty($originalAccount) && !empty($currentAccount))
                    {
                        return sprintf($arrayMsgs[3], $currentAccount, $originalAccount);
                    }
                    else
                        return $arrayMsgs[0];
                }
            }
            break;

        case 'Senders':
            {
                $apiResult = str_replace('[pending]', '[pending]<br>', $apiResult);
                $apiResult = str_replace('[active]', '<br>[active]<br>', $apiResult);
                $apiResult = str_replace('[notActive]', '<br>[notActive]<br>', $apiResult);
                return $apiResult;
            }
            break;

        case 'Normal':
            if($apiResult{0} != '#')
                return $arrayMsgs[$apiResult];
            else
                return $apiResult;
            break;
    }
}

/**
 * Upload Path
 * @return string
 */
function orderType($order_type)
{
    if($order_type == 'contract'){

        return 'صياغة عقد';
    }elseif ($order_type == 'consultation'){

        return 'مراجعه واستشارة';
    }elseif ($order_type == 'special'){
        return 'طلب خاص';
    }
}

function orderPayType($order_pay)
{
    if($order_pay == 'cash'){

        return 'كاش';
    }elseif ($order_pay == 'paypal'){

        return 'باي بال';
    }elseif ($order_pay == 'file'){
        return 'حواله بنكيه';
    }else{
        return 'لم تحدد بعد';
    }
}



function orderStatus($order_status)
{
    if($order_status == 'new'){

        return 'جديد';

    }elseif ($order_status == 'review'){

        return 'تحت المراجعة';
    }elseif ($order_status == 'payed'){
        return 'مدفوع';
    }elseif ($order_status == 'canceled'){
        return 'تم الغاءوه';
    }elseif ($order_status == 'finish'){
        return 'منتهي';
    }
}


/**
 * Upload Path
 * @return string
 */
function uploadpath()
{
    return 'photos';
}

/**
 * Get Image
 * @param $filename
 * @return string
 */
function getimg($filename)
{
    $base_url = url('/');
    if($filename)
    return $base_url.'/storage/'.$filename;
    else{
        return  $base_url.'/profile-placeholder.png';
    }
}


/**
 * Upload an image
 * @param $img
 */
function uploader($request,$img_name)
{
    $path = \Storage::disk('public')->put(uploadpath(), $request->file($img_name));
    return $path;
}


function uploaderImage($img_name){

    $path = \Storage::disk('public')->put(uploadpath(), ($img_name));
    return $path;
}

function status()
{
    $array = [
        '1'=>'مفعل',
        '0'=>'غير مفعل',
    ];
    return $array;
}

function SendEmail($email,$name,$verification_code){

    $numbers = $email;						   	//the mobile number or set of mobiles numbers that the SMS message will be sent to them, each number must be in international format, without zeros or symbol (+), and separated from others by the symbol (,).

    $msg = " كود التفعيل   ".$verification_code;

    $MsgID = rand(1,99999);


    // send card to user mail
    $htmlStr = "";
    $htmlStr .= "Hi " . $name . ",<br /><br />";

    $htmlStr .= "مرحبا بك في صياغه <br /><br /><br />";
    $htmlStr .= "<a style='padding:1em;
 font-weight:bold;
  background-color:blue;
   color:#fff;'>كود التفعيل الخاص بك هو</a><br /><br /><br />";
    $htmlStr .= "
      
            <div style='display:
             block; width: 200px;
              height: 30px;
               text-align: 
               center; font-size: 14px'> 
               $verification_code 
               
               </div> ";


    $htmlStr .= "مع واجب التحية والتقدير ,<br />";
    $name = "mohamed";
    $email_sender = "mohamed01148808417@gmail.com";

    // password mail Mohamed01148808417@

    $subject = "التفعيل الخاص بك ";
    $recipient_email = $email;

    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8";

    $headers .= "From: {$name} <{$email_sender}> \n";

    $body = $htmlStr;

    mail($recipient_email, $subject, $body, $headers);


}
