<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.admin.index')->with('users',User::where('is_admin',1)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries=\App\Country::pluck('ar_name','id')->toArray();
        $categories=\App\Category::pluck('ar_name','id')->toArray();
        return view('admin.admin.add',['categories'=>$categories,'countries'=>$countries]);
    }

    public function show ($id){

       return "عرض";

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'image'=>'required|image|',
            'name'=>'required|string|',
            'email'=>'required||string|email|max:255|unique:users',
            'phone'=>'required||string|max:255|unique:users',
            'identity'=>'required||string|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'job'=>'nullable|string|',
            'gender'=>'nullable|string|',
            'birth_date'=>'nullable|string|date_format:m/d/Y',
            'facebook_link'=>'nullable|string|url',
            'instagram_link'=>'nullable|string|url',
        ]);
        $image = uploader($request,'image');
        $inputs=$request->all();
        $inputs['image']=$image;
        $inputs['is_admin']=1;
        $inputs['password']=Hash::make($request->password);

        User::create($inputs);
        alert()->success('تم اضافة الادمن بنجاح !')->autoclose(5000);
        return back();

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries=\App\Country::pluck('ar_name','id')->toArray();
        $user= User::find($id);
        $country_id=$user->Region->city->country->id;
        $cities=\App\City::where('country_id',$country_id)->pluck('ar_name','id')->toArray();
        $city_id=$user->Region->city->id;
        $regions=\App\Region::where('city_id',$city_id)->pluck('ar_name','id')->toArray();

        return view('admin.admin.edit',['regions'=>$regions,'city_id'=>$city_id,'country_id'=>$country_id,'cities'=>$cities,'countries'=>$countries])->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $user=User::find($id);

        $this->validate($request,[
            'image'=>'sometimes|image|',
            'name'=>'required|string|',
            'identity'=>'required|string|',
            'email'=>'nullable||string|email|max:255|unique:users,email,'.$user->id,
            'phone'=>'nullable||numeric|unique:users,phone,'.$user->id,
            'password' => 'nullable|string|min:6|confirmed',
            'job'=>'nullable|string|',
            'gender'=>'nullable|string|',
            'birth_date'=>'nullable|string|date_format:m/d/Y',
            'facebook_link'=>'nullable|string|url',
            'instagram_link'=>'nullable|string|url',
        ]);

        $inputs = $request->all();
        if ($request->image != null)
        {
            if ($request->hasFile('image')) {
                $picture = uploader($request,'image');
                $user->update(['image' => $picture]);
            }
        }
        if($request->password != null) {$user->update(['password'=>Hash::make($request->password)]);}
        $user->update(array_except($inputs,['password','image']));
        alert()->success('تم تعديل بيانات الادمن بنجاح !')->autoclose(5000);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);

        if($user->email=='admin@admin.com'){
            alert()->error('لايمكن حذف الادمن  ');
            return back();
        }
        if ($user){
            deleteImg($user->image);
            $user->delete();
            alert()->success('تم حذف الادمن بنجاح');
            return back();
        }
        alert()->error('الادمن الذى تحاول حذفه غير موجود');
        return back();
    }


    public function login(){
        return view('admin.login');
    }
    public function doLogin(Request $request){
        $email = $request->get('email');
        $password = $request->get('password');

        // check is true user is admin
        if (\Auth::attempt(['email' => $email, 'password' => $password]) && \Auth::user()->is_admin == 1) {
            return view('admin.main');
        }else{
            session()->flash('error','من فضلك ادخل البيانات صحيحة ');
            return back();

        }
    }


    public function logout(Request $request) {
        \Auth::logout();
        return redirect()->route('show_login');
    }
}
