<?php

namespace App\Http\Controllers\Admin;

use App\Contact_Us;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.contact.index')->with('contacts',Contact_Us::all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function show($id){

        return view('admin.contact.show')->with('contact',Contact_Us::find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact_us = Contact_Us::find($id);

        if ($contact_us){
            $contact_us->delete();
            alert()->success('تم حذف الرساله بنجاح');
            return back();
        }
        alert()->error(' الذى تحاول حذفه غير موجود');
        return back();
    }


}
