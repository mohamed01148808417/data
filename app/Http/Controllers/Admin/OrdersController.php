<?php

namespace App\Http\Controllers\Admin;

use App\Consultation;
use App\Contract;
use App\Order;
use App\Special;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class OrdersController extends Controller
{

    public function changeStatus(Request $request){

        $order_id = $request->order_id;
        $status = $request->status;

        $order  = Order::find($order_id);


        if($order){
            $order->status = $status;
            $order->save();
        }

        alert()->success('تم تعديل حاله الطلب  بنجاح !')->autoclose(5000);


        return back();


    }

    public function sendingEmail(Request $request){

        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);
        try {
            // Server settings
            $mail->SMTPDebug = 1;                                	// Enable verbose debug output
            //$mail->isSMTP();                                     	// Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';					// Specify main and backup SMTP servers
            $mail->SMTPAuth = false;                              	// Enable SMTP authentication
            $mail->SMTPAutoTLS = false;
            $mail->Username = 'mohamed01148808417@gmail.com';             // SMTP username
            $mail->Password = '2731996MAlove';              // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            $mail->CharSet = 'utf-8';                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('mohamed01148808417@gmail.com', 'Mailer');
            $mail->addAddress($request->user_email, $request->user_name);	// Add a recipient, Name is optional
            $mail->addReplyTo('mohamed01148808417@gmail.com', 'Mailer');
            $mail->addCC($request->user_email);
            $mail->addBCC($request->user_email);

            //Attachments (optional)
            // $mail->addAttachment('/var/tmp/file.tar.gz');			// Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');	// Optional name

            //Content
            $mail->isHTML(true); 																	// Set email format to HTML
            $mail->Subject = $request->title;
            $mail->Body    = "<p>$request->msg</p>";// message

            $mail->send();
            alert()->success('تم ارسال البريد بنجاح');
            return back();
        } catch (Exception $e) {


            alert()->error('لم نتمكن من ارسال البريد حتي الان تأكد من البيانات');
            return back();
        }


        alert()->error('لم نتمكن من ارسال البريد حتي الان تأكد من البيانات');
        return back();


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->order_type){
          if($request->order_type == 'contract'){
              $orders = Order::orderBy('id','DESC')->where('order_type','contract')->get();
          }elseif ($request->order_type == 'consultation'){
              $orders = Order::orderBy('id','DESC')->where('order_type','consultation')->get();
          }else{
              $orders = Order::orderBy('id','DESC')->where('order_type','special')->get();
          }
        }else{

           $orders = Order::orderBy('id','DESC')->get();
        }

        return view('admin.orders.index')->with('orders',$orders);
    }

    public function show ($id){
        $order = Order::find($id);



        if($order->order_type == 'contract'){
            $contract = Contract::where('order_id',$order->id)->first();

        }elseif ($order->order_type == 'consultation'){
            $consultation = Consultation::where('order_id',$order->id)->first();
        }elseif ($order->order_type == 'special'){
            $special = Special::where('order_id',$order->id)->first();
        }

        return view('admin.orders.show',compact('order','contract','consultation','special'));


    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user= User::find($id);

        return view('admin.users.edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $user=Order::find($id);

        $this->validate($request,[
            'image'=>'sometimes|image|',
            'name'=>'required|string|',
            'email'=>'nullable||string|email|max:255|unique:users,email,'.$user->id,
            'phone'=>'nullable||numeric|unique:users,phone,'.$user->id,
            'password' => 'nullable|string|min:6|confirmed',

            ]);

        $inputs = $request->all();

        if ($request->image != null)
        {
            if ($request->hasFile('image')) {
                $picture = uploader($request,'image');
                $user->update(['image' => $picture]);
            }
        }
        if($request->password != null) {$user->update(['password'=>Hash::make($request->password)]);}
        $user->update(array_except($inputs,['password','image']));
        alert()->success('تم تعديل بيانات  بنجاح !')->autoclose(5000);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=Order::find($id);
        // delete
        if ($user){
            $user->delete();
            alert()->success('تم حذف الطلب بنجاح');
            return back();
        }

        alert()->error(' الذى تحاول حذفه غير موجود');
        return back();
    }


    public function login(){
        return view('admin.login');
    }
    public function doLogin(Request $request){
        $email = $request->get('email');
        $password = $request->get('password');

        // check is true user is admin
        if (\Auth::attempt(['email' => $email, 'password' => $password]) && \Auth::user()->is_admin == 1) {
            return view('admin.main');
        }else{
            session()->flash('error','من فضلك ادخل البيانات صحيحة ');
            return back();

        }
    }


    public function logout(Request $request) {
        \Auth::logout();
        return redirect()->route('show_login');
    }
}
