<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\City;
use App\Contact_Us;
use App\Country;
use App\Http\Controllers\Controller;
use App\Region;
use App\Setting;
use App\User;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function terms($id){

        $setting = Setting::find($id);

        return view('admin.terms')->with('setting',$setting);


    }


    public function updateSettings(Request $request){

        $setting = Setting::find(1);

        $setting->terms = $request->terms;

        $setting->save();


        alert()->success('تم تحديث الشروط والاحكام بنجاح');
        return redirect()->back();


    }
//    public function sendNotification(){
//        $user = User::first();
//
//        $details = [
//            'greeting' => 'Hi Artisan',
//            'body' => 'This is my first notification from ItSolutionStuff.com',
//            'thanks' => 'Thank you for using ItSolutionStuff.com tuto!',
//            'actionText' => 'View My Site',
//            'actionURL' => url('/'),
//            'order_id' => 101
//        ];
//
//        Notification::send($user, new MyFirstNotification($details));
//
//        dd('done');
//    }
}
