<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index')->with('users',User::where('is_admin',0)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.add');
    }

    public function show ($id){

       return "عرض";

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'image'=>'required|image|',
            'name'=>'required|string|',
            'email'=>'required||string|email|max:255|unique:users',
            'phone'=>'required||string|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $image = uploader($request,'image');
        $inputs=$request->all();
        $inputs['image']=$image;
        $inputs['password']=Hash::make($request->password);
        User::create($inputs);
        alert()->success('تم اضافة المستخدم بنجاح !')->autoclose(5000);
        return back();

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user= User::find($id);

        return view('admin.users.edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $user=User::find($id);

        $this->validate($request,[
            'image'=>'sometimes|image|',
            'name'=>'required|string|',
            'email'=>'nullable||string|email|max:255|unique:users,email,'.$user->id,
            'phone'=>'nullable||numeric|unique:users,phone,'.$user->id,
            'password' => 'nullable|string|min:6|confirmed',

            ]);

        $inputs = $request->all();

        if ($request->image != null)
        {
            if ($request->hasFile('image')) {
                $picture = uploader($request,'image');
                $user->update(['image' => $picture]);
            }
        }
        if($request->password != null) {$user->update(['password'=>Hash::make($request->password)]);}
        $user->update(array_except($inputs,['password','image']));
        alert()->success('تم تعديل بيانات  بنجاح !')->autoclose(5000);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);

        if($user->email=='admin@admin.com'){
            alert()->error('لايمكن حذف الادمن  ');
            return back();
        }
        if ($user){
            $user->delete();
            alert()->success('تم حذف المستخدم بنجاح');
            return back();
        }
        alert()->error(' الذى تحاول حذفه غير موجود');
        return back();
    }


    public function login(){
        return view('admin.login');
    }
    public function doLogin(Request $request){
        $email = $request->get('email');
        $password = $request->get('password');

        // check is true user is admin
        if (\Auth::attempt(['email' => $email, 'password' => $password]) && \Auth::user()->is_admin == 1) {
            return view('admin.main');
        }else{
            session()->flash('error','من فضلك ادخل البيانات صحيحة ');
            return back();

        }
    }


    public function logout(Request $request) {
        \Auth::logout();
        return redirect()->route('show_login');
    }
}
