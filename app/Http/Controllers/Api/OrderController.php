<?php

namespace App\Http\Controllers\Api;

use App\Commitment;
use App\Consultation;
use App\Contract;
use App\Http\Resources\NotificationResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrdersResource;
use App\Http\Resources\SingleOrderResource;
use App\Http\Resources\UserProviderResource;
use App\Http\Resources\UserResource;
use App\Notification;
use App\Order;
use App\OrderImage;
use App\ProviderProject;
use App\Special;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponses;
use Tymon\JWTAuth\JWTAuth;
use Validator;
use Illuminate\Http\Response;
class OrderController extends Controller
{
    use ApiResponses;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function GetAllProviderAcceptedOrdered()
    {
        $Accepted = Order::where('provider_id',auth()->id())->where('status','accepted')->latest("updated_at")->paginate(15);
        return $this->apiResponse(new OrderResource($Accepted));
    }

    public function GetAllProviderAllOrdered()
    {
        $Accepted = Order::where('provider_id',auth()->id())->latest("updated_at")->paginate(15);
        return $this->apiResponse(new OrderResource($Accepted));
    }

    public function OrderProviderUserDetails($order_id)
    {
        $order = Order::find($order_id);
        return $this->apiResponse(new UserProviderResource($order));
    }


    public function GetAllProviderPendingOrdered()
    {
        $pending = Order::where('provider_id',auth()->id())->where('status','pending')->latest("updated_at")->paginate(15);
        return $this->apiResponse(new OrderResource($pending));
    }


    public function GetAllProviderCanceledOrdered()
    {
        $canceled = Order::where('provider_id',auth()->id())->where('status','canceled')->latest("updated_at")->paginate(15);
        return $this->apiResponse(new OrderResource($canceled));
    }


    public function GetAllProviderFinishedOrdered()
    {
        $canceled = Order::where('provider_id',auth()->id())->where('status','finished')->latest("updated_at")->paginate(15);
        return $this->apiResponse(new OrderResource($canceled));
    }

    /**
     * Display a listing of the user ordered.
     *
     *
     */

    public function GetAllUserAcceptedOrdered()
    {
        $Accepted = Order::where('user_id',auth()->id())->where('status','accepted')->latest("updated_at")->paginate(15);
        return $this->apiResponse(new OrderResource($Accepted));
    }

   public function GetAllUserAllOrdered()
    {
        $Accepted = Order::where('user_id',auth()->id())->latest("updated_at")->paginate(15);
        return $this->apiResponse(new OrderResource($Accepted));
    }

    public function GetAllUserPendingOrdered()
    {
        $pending = Order::where('user_id',auth()->id())->where('status','pending')->latest("updated_at")->paginate(15);
        return $this->apiResponse(new OrderResource($pending));
    }

    public function GetAllUserCanceledOrdered()
    {
        $canceled = Order::where('user_id',auth()->id())->where('status','canceled')->latest("updated_at")->paginate(15);
        return $this->apiResponse(new OrderResource($canceled));
    }

    public function GetAllUserFinishedOrdered()
    {
        $canceled = Order::where('user_id',auth()->id())
            ->where(function($q) {
                $q->where('status', 'finished')
                    ->orWhere('status', 'canceled');
            })->latest("updated_at")->paginate(15);
        return $this->apiResponse(new OrderResource($canceled));
    }

    public function Addproject(Request $request){

        $rules = array(
            'title' =>'required|string|max:191',
            'description'   =>'nullable|string',
            'file'   =>'nullable|file',
        );
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}
            $inputs=$request->all();
            if($request->hasFile('file')){
                $image = uploader($request,'file');
                $inputs['file']=$image;
            }
            $inputs['user_id']=auth()->id();
            $project=ProviderProject::create($inputs);
        $project->file=getimg($project->file);

        return $this->createdResponse($project);
    }

    public function EditProject(Request $request,$id){
        $project=ProviderProject::find($id);

        if(!$project){
            return \response()->json([
                'status'    => false,
                'message'   => ' المشروع غير موجود'
            ],400);
        }
        if($project->user_id!=auth()->id())
            return \response()->json([
                'status'    => false,
                'message'   => ' لا تمتلك هذا المشروع'
            ],400);

        $rules = array(
            'title' =>'required|string|max:191',
            'description'   =>'nullable|string',
            'file'   =>'nullable|file',
        );
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}
            $inputs=$request->all();
            if($request->hasFile('file')){
                $image = uploader($request,'file');
                $inputs['file']=$image;
            }
            $inputs['user_id']=auth()->id();
            $project->update($inputs);

        $project->file=getimg($project->file);
            return $this->createdResponse($project);
    }

    public function Getproject($id){

        $project=ProviderProject::find($id);

        if(!$project){
            return \response()->json([
                'status'    => false,
                'message'   => 'please make sure that id is true'
            ],404);
        }
        if($project->user_id != auth()->id())
            return $this->unKnowError();

        $project->file=getimg($project->file);
        return $this->createdResponse($project);
    }
    public function GetProviderprojects(){

        $projects=ProviderProject::where('user_id',auth()->id())->get();

        foreach ($projects as $project){
            $project->file=getimg($project->file);

        }
        return $this->createdResponse($projects);
    }

    public function DeleteProject($id){

        $project=ProviderProject::findOrFail($id);

        if($project->user_id!=auth()->id())
            return $this->unKnowError();

        $project->delete();
        return $this->createdResponse($project);
    }

    public function SendOrder(Request $request){


        $user = \JWTAuth::user();
        if(!$user){

            return \response()->json([
                'status'    => 'false',
                'msg'       => 'token required'
            ],401);
        }

        $rules = [
            'order_type' =>'required|string|max:191',
            'has_files'  =>'nullable',
            'images' => 'nullable',
            'images.*' => 'image|mimes:jpeg,png,jpg|max:2048',
            'execution_time'    => 'required',
            'amount'            => 'required',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        if($request->order_type == 'contract'){
            // عقد صياغه
            $rules = [
                'details'           =>'required',
                'contract_time'     =>'nullable',
                'p_one_name'            => 'required',
                'p_one_national'            => 'required',
                'p_one_card'            => 'required',
                'p_one_city'            => 'required',
                'p_one_region'            => 'required',
                'p_one_address'            => 'required',
                'p_one_email'            => 'required',
                'p_one_phone'            => 'required',
                'p_two_name'            => 'required',
                'p_two_national'            => 'required',
                'p_two_card'            => 'required',
                'p_two_city'            => 'required',
                'p_two_region'            => 'required',
                'p_two_address'            => 'required',
                'p_two_email'            => 'required',
                'p_two_phone'            => 'required',
                'special_terms'            => 'required',
                'partial_terms'            => 'required',
                'other_terms'            => 'required',
                'termination_of_contract'            => 'required',
                'contract_of_law'            => 'required',
                'commitments'            => 'required',

            ];

            $validation=$this->apiValidation($request,$rules);
            if($validation instanceof Response){return $validation;}


            // create order and contract here

            $order = Order::create([
                'order_type'    => $request->order_type,
                'has_files'     => $request->has_files,
                'user_id'       => \JWTAuth::user()->id,
                'execution_time'    => $request->execution_time,
                'amount'        => $request->amount,
                'status'        => 'New',
                'created_at'    => Carbon::now()
            ]);

            if($order->has_files == 1){
                foreach ($request->images as $image){

                    $image = OrderImage::create([
                        'order_id'  => $order->id,
                        'image'     => uploaderImage($image)
                    ]);
                }
            }



            $contract = Contract::create([
                'order_id'          => $order->id,
                'details'           => $request->details,
                'contract_time'     => $request->contract_time,
                'p_one_name'        => $request->p_one_name,
                'p_one_national'    => $request->p_one_national,
                'p_one_card'        => $request->p_one_card,
                'p_one_city'        => $request->p_one_city,
                'p_one_region'      => $request->p_one_region,
                'p_one_address'     => $request->p_one_address,
                'p_one_email'       => $request->p_one_email,
                'p_one_phone'       => $request->p_one_phone,
                'p_two_name'        => $request->p_one_name,
                'p_two_national'    => $request->p_one_national,
                'p_two_card'        => $request->p_one_card,
                'p_two_city'        => $request->p_one_city,
                'p_two_region'      => $request->p_one_region,
                'p_two_address'     => $request->p_one_address,
                'p_two_email'       => $request->p_one_email,
                'p_two_phone'       => $request->p_one_phone,
                'special_terms'     => $request->special_terms,
                'partial_terms'     => $request->partial_terms,
                'other_terms'       => $request->other_terms,
                'termination_of_contract'   => $request->termination_of_contract,
                'contract_of_law'   => $request->contract_of_law,
                'created_at'        => Carbon::now(),
            ]);


            foreach ($request->commitments as $commitment){

                Commitment::create([
                    'contract_id'   => $contract->id,
                    'commit'        => $commitment
                ]);
            }

            // return contract
            $orderResource = new SingleOrderResource($order);

            return $this->apiResponse($orderResource);


        }else if($request->order_type == 'consultation'){

            // create order and consultation  here

            $rules = [
                'details'           =>'required',
            ];

            $validation=$this->apiValidation($request,$rules);
            if($validation instanceof Response){return $validation;}


            // create order and contract here

            $order = Order::create([
                'order_type'    => $request->order_type,
                'has_files'     => $request->has_files,
                'user_id'       => \JWTAuth::user()->id,
                'execution_time'    => $request->execution_time,
                'amount'        => $request->amount,
                'status'        => 'New',
                'created_at'    => Carbon::now()
            ]);

            if($order->has_files == 1){
                foreach ($request->images as $image){

                    $image = OrderImage::create([
                        'order_id'  => $order->id,
                        'image'     => uploaderImage($image)
                    ]);
                }
            }

            $consultation = Consultation::create([
                'order_id'          => $order->id,
                'details'           => $request->details,
                'created_at'        => Carbon::now(),
            ]);

            // return contract
            $orderResource = new SingleOrderResource($order);

            return $this->apiResponse($orderResource);


        }else if($request->order_type == 'special'){


            // create order and special  here

            $rules = [
                'details'           =>'required',
            ];

            $validation=$this->apiValidation($request,$rules);
            if($validation instanceof Response){return $validation;}


            // create order and contract here

            $order = Order::create([
                'order_type'    => $request->order_type,
                'has_files'     => $request->has_files,
                'user_id'       => \JWTAuth::user()->id,
                'execution_time'    => $request->execution_time,
                'amount'        => $request->amount,
                'status'        => 'New',
                'created_at'    => Carbon::now()
            ]);

            if($order->has_files == 1){
                foreach ($request->images as $image){
                    $image = OrderImage::create([
                        'order_id'  => $order->id,
                        'image'     => uploaderImage($image)
                    ]);
                }
            }

            $special = Special::create([
                'order_id'          => $order->id,
                'details'           => $request->details,
                'created_at'        => Carbon::now(),
            ]);

            // return contract
            $orderResource = new SingleOrderResource($order);

            return $this->apiResponse($orderResource);
        }else{

            // return error

            return \response()->json([
                'status'    => 'false',
                'msg'       => 'order type must be on of them [contract, consultation,special]'
            ],400);

        }
    }



    public  function cancelOrder(Request $request){

        $rules = [
            'order_id' =>'required',
            'reason'=>'required|string'
        ];
        $validation=$this->apiValidation($request,$rules);

        if($validation instanceof Response){return $validation;}

        $order=Order::find($request->order_id);


        //dd($order);

        if($order){
            if($order->user_id==auth()->id()){

                $user=User::find($order->provider_id);


                $order->status='canceled';
                $order->save();

                $notfication = new \App\Notification();
                $notfication->title=" تم الغاء الطلب من قبل العميل";
                $notfication->type='order';
                $notfication->user_id=$order->user_id;
                $notfication->item_id=$order->id;
                $notfication->save();
                $result="";
                if ($user->fcm_token_android)
                     $result = $this->notifyByFirebase(' تم الغاء الطلب من قبل العميل',
                         $order->title,[$user->fcm_token_android],['type'=>'order','data'=>$order],false);
                if ($user->fcm_token_ios)
                     $result = $this->notifyByFirebase(' تم الغاء الطلب من قبل العميل',$order->title,[$user->fcm_token_ios],['type'=>'order','data'=>$order],true);
                $data['order']=$order;
                $data['result']=$result;
                return $this->createdResponse($data);
            }
            elseif($order->provider_id==auth()->id()){

                $user=User::find($order->user_id);
                $order->status='canceled';
                $order->save();
                $notfication = new \App\Notification();
                $notfication->title=" تم الغاء الطلب من قبل مزود الخدمة";
                $notfication->type='order';
                $notfication->user_id=$order->user_id;
                $notfication->item_id=$order->id;
                $notfication->save();
                $result="";
                if ($user->fcm_token_android)
                $result = $this->notifyByFirebase(' تم الغاء الطلب من قبل مزود الخدمة',$order->title,[$user->fcm_token_android],['type'=>'order','data'=>$order],false);
                if ($user->fcm_token_ios)
                $result = $this->notifyByFirebase(' تم الغاء الطلب من قبل مزود الخدمة',$order->title,[$user->fcm_token_ios],['type'=>'order','data'=>$order],true);
                $data['order']=$order;
                $data['result']=$result;
                return $this->createdResponse($data);
            }
            else{
                return $this->apiResponse(__(' غير مصرح  '));
            }
        }else{
            return $this->apiResponse(__('الطلب غير موجود '));

        }


    }

    public  function finishOrder(Request $request){
        $rules = [
            'order_id' =>'required',
        ];

        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        $order=Order::find($request->order_id);

        if($order){

            if($order->provider_id==auth()->id()){
                if(Carbon::createFromFormat('Y-m-d H:i:s',
                        $order->date.''.$order->time) <=   Carbon::now())
                    {
                        $user=User::find($order->user_id);
                        $order->status='finished';
                        $order->save();
                        $notfication = new \App\Notification();
                        $notfication->title=" تم انهاء الطلب من قبل مزود الخدمة";
                        $notfication->type='order';
                        $notfication->user_id=$order->user_id;
                        $notfication->item_id=$order->id;
                        $notfication->save();
                        $result="";
                        if ($user->fcm_token_android)
                        $result = $this->notifyByFirebase(' تم انهاء الطلب من قبل مزود الخدمة',$order->title,[$user->fcm_token_android],['type'=>'order','data'=>$order],false);
                        if ($user->fcm_token_ios)
                        $result = $this->notifyByFirebase(' تم انهاء الطلب من قبل مزود الخدمة',$order->title,[$user->fcm_token_ios],['type'=>'order','data'=>$order],false);
                        $data['order']=$order;
                        $data['result']=$result;
                        return $this->createdResponse($data);
                }
                else{
                    return $this->apiResponse(null,__('الوقت لم ينتهى بعد '),401);
                }
            }
            else{
                return $this->apiResponse(__(' غير مصرح  '));
            }
        }else{
            return $this->apiResponse(__('الطلب غير موجود '));
        }


    }

    public function rateOrder(Request $request){

        $rules = [
            'order_id' =>'required|string|max:191',
            'comment'=>'required|string',
            'rate_no'=>'required|numeric|max:5'
        ];

        $validation=$this->apiValidation($request,$rules);

        if($validation instanceof Response){return $validation;}

        $order=Order::find($request->order_id);

        if($order){
            if($order->user_id==auth()->id()){
                $order->comment=$request->comment;
                $order->rate=$request->rate_no;
                $order->save();
                $msg=" لقد تم تقيمك ب  " .$order['rate'];
                $user=User::find($order->provider_id);
                if ($user->fcm_token_android)
                    $result = $this->notifyByFirebase($msg,$order->title,[$user->fcm_token_android],['type'=>'comment','data'=>$order],false);
                if ($user->fcm_token_ios)
                     $result = $this->notifyByFirebase($msg,$order->title,[$user->fcm_token_ios],['type'=>'comment','data'=>$order],true);
                return $this->createdResponse($order);

            }
            else{
                return $this->apiResponse(__(' غير مصرح  '));
            }
        }else{
            return $this->apiResponse(__('الطلب غير موجود '));
        }


    }

    public  function AcceptOrder(Request $request){
        $rules = [
            'order_id' =>'required',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}
        $order=Order::find($request->order_id);
        if($order) {
            if ($order->provider_id == auth()->id()) {
                $user = User::find($order->user_id);
                $order->status = 'accepted';
                $order->save();
                $notfication = new \App\Notification();
                $notfication->title = "تم قبول الطلب  ";
                $notfication->type = 'order';
                $notfication->user_id = $order->user_id;
                $notfication->item_id = $order->id;
                $notfication->save();
                $result = '';


                if ($user->fcm_token_android)
                    $result = $this->notifyByFirebase(' تم قبول الطلب',
                        $order->title, [$user->fcm_token_android],
                        ['type' => 'order', 'data' => $order],
                        false);
                if ($user->fcm_token_ios)
                    $result = $this->notifyByFirebase(' تم قبول الطلب', $order->title, [$user->fcm_token_ios], ['type' => 'order', 'data' => $order], true);


                $data['order'] = $order;
                $data['result'] = $result;
                $data['fcm_token_android'] = $user->fcm_token_android;
                return $this->apiResponse($data);
            } else {
                return $this->apiResponse(__('غي مصرح'));
            }
        }
        else{
            return $this->apiResponse(__('الطلب غير موجود '));

        }

     }

    public function  getNotification(){
        $notfication =  \App\Notification::where('read','no')->where('user_id',auth()->id())->paginate();
        return $this->apiResponse(new NotificationResource($notfication));
    }

    public function  getNotificationCount(){
        $notfications =  Notification::where('read','no')->where('user_id',auth()->id())->count();
        return $this->apiResponse($notfications);
    }

    public function  showNotification($id){
        $notfication =  \App\Notification::find($id);
        if($notfication){
            $notfication->read='yes';
            $notfication->save();
            return $this->apiResponse(__('تم قراءته '));
        }else{
            return $this->apiResponse(__('الاشعار غير موجود '));
        }
    }

    public function notification($token, $title)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;

        $notification = [
            'title' => $title,
            'sound' => true,
        ];

        $extraNotificationData = ["message" => $notification,
            "moredata" =>'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh '];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AIzaSyC_T6iu_4_TsGwGjCYtUuEs3252JW7W4-Q',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function notifyByFirebase($title,
                                     $body,
                                     $tokens,
                                     $data = [] ,
                                     $is_notification = true)
    {
        // https://gist.github.com/rolinger/d6500d65128db95f004041c2b636753a
        // API access key from Google FCM App Console
        // env('FCM_API_ACCESS_KEY'));

        //    $singleID = 'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd';
        //    $registrationIDs = array(
        //        'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd',
        //        'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd',
        //        'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd'
        //    );
        $registrationIDs = $tokens;

        // prep the bundle
        // to see all the options for FCM to/notification payload:
        // https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support

        // 'vibrate' available in GCM, but not in FCM
        $fcmMsg = array(
            'body' => $body,
            'title' => $title,
            'sound' => "default",
            'color' => "#203E78"
        );
        // I haven't figured 'color' out yet.
        // On one phone 'color' was the background color behind the actual app icon.  (ie Samsung Galaxy S5)
        // On another phone, it was the color of the app icon. (ie: LG K20 Plush)

        // 'to' => $singleID ;      // expecting a single ID
        // 'registration_ids' => $registrationIDs ;     // expects an array of ids
        // 'priority' => 'high' ; // options are normal and high, if not set, defaults to high.
        $fcmFields = array(
            'registration_ids' => $registrationIDs,
            'priority' => 'high',
            'data' => $data
        );
        if ($is_notification)
        {
            $fcmFields['notification'] = $fcmMsg;
        }

        //  'Authorization: key=AAAAq3alF2Q:APA91bFuBZPAKUKT0QjG_KQQXBUGYGb3KKwtdB2YCyTEUF_MC7oehaoalBzAGRpJ6EauhcoBHi02FEy0_fPfJTibh2nPj0GWYnE8QRy-3oRDE105DGC8pp_pFkXtyaP2weJwmcKTJYQd',
        $headers = array(
            'Authorization: key=AAAANLaLRLM:APA91bHzUSCCZ5zaryOCg_6Jh7a5S61LQbqFzEVE4alKrDuqgcIWqMAw_dTxOO6Wo8n_uXhoOOobzvIurArwievayfEWGUW0m-z_xqi0wUtmod2Rgvwm3z9afrfOxMwtX_stZMhvSwwV',
            'Content-Type: application/json'
        );

        /*        info("API_ACCESS_KEY_client: ".env('API_ACCESS_KEY_client'));
               info("PUSHER_APP_ID: ".env('PUSHER_APP_ID'));*/

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function orderStatus($order_type){

        $user = \JWTAuth::user();
        if(!$user){

            return \response()->json([
                'status'    => 'false',
                'msg'       => 'token required'
            ],401);
        }

        $orders = Order::where('order_type',$order_type)->where('user_id',$user->id)->whereIn('status',['new','review','payed','finished'])->paginate(16);

        if($orders){

            $ordersResource = new OrdersResource($orders);

            return $this->apiResponse($ordersResource);
        }

        return $this->apiResponse(null,__(' غير موجود '));


    }


    public function orderPay(Request $request){

        $inputs = $request->all();

        $user = \JWTAuth::user();

        if(!$user){


            return \response()->json([
                'status'    => false,
                'msg'       => 'راجع تسجيل الدخول'
            ],401);
        }

        $order = Order::where('id',$inputs['order_id'])->where('user_id',$user->id)->first();

        if(!$order){
            // return false

            return \response()->json([
                'status'    => false,
                'msg'       => 'الطلب غير موجود'
            ],400);
        }

        if($inputs['pay']   ==  'cash'){

            $order->pay = 'cash';
            $order->status  = 'review';

            $order->save();

            return \response()->json([
                'status'    => true,
                'msg'       => 'سيتم مراجعه الكاش من المدير ومراسلتك عبر البريد'
            ],200);

        }elseif ($inputs['pay'] == 'file') {
            $rules = [
                'file_payed' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ];
            $validation = $this->apiValidation($request, $rules);

            if ($validation instanceof Response) {
                return $validation;
            }
            $order->file_payed = uploader($request, 'file_payed');
            $order->pay = 'file';
            $order->status  = 'review';
            $order->save();
            return \response()->json([
                'status'    => true,
                'msg'       => 'سيتم مراجعه الملف المرسل من المدير ومراسلتك عبر البريد'
            ],200);
        }else{

            return \response()->json([
                'status'    => false,
                'msg'       => 'من فضلك اختر طريقه دفع صحيحه'
            ],400);

        }
    }


    public function getSingleOrder($order_id){

        $user = \JWTAuth::user();

        if(!$user){
            return \response()->json([
                'status'    => false,
                'msg'       => 'من فضلك اختر طريقه دفع صحيحه'
            ],400);

        }

    }
}
