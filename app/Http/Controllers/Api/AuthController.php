<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SingleUserResource;
use App\Http\Resources\UserResource;
use App\Http\Traits\ApiResponses;
use App\Http\Traits\UserOperation;
use App\Notifications\ConfirmationEmail;
use App\Notifications\ValidationEmail;
use App\UserSubCategory;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use JWTFactory;
use JWTAuth;
use Validator;
use Illuminate\Http\Response;


class AuthController extends Controller
{
    use ApiResponses,UserOperation;

    /**
     * Regular Registration
     *
     * @param Request $request
     * @param null $facebook
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request)
    {
        $rules = [
            'name'       =>'required|string|max:191',
            'address'    =>'required|string|max:191',
            'email'      =>'required|email:max:191|unique:users',
            'phone'      =>'required|string|max:191|unique:users',
            'password'   =>'required|string|max:191|min:6',
            'image'      =>'nullable|image|mimes:jpg,jpeg,gif,png',
        ];


        $validation=$this->apiValidation($request,$rules);

        if($validation instanceof Response){return $validation;}

        if($request->hasfile('image')){

            $img=uploader($request, 'image');
//                dd($request['image']);
            $data['image']=$img;
//                dd(2);
        }

        $user =$this->RegisterUser($request);

        $user = User::where('id',$user->id)->first();
        $token = JWTAuth::fromUser($user);
        $user['token'] = $token;
        $user = new UserResource($user);
        return $this->apiResponse($user);

        /*return \response()->json([
            'status'    => true,
            'user'      => new UserResource($user),
            'message'   => "من فضلك ادخل كود التفعيل المرسل علي الرقم الخاص بك "
        ],200);*/
//        $msq=" Sheari Application Verification code : " . $user->verification_code;
//        sendSms($msq,$user->phone);
        //$token = JWTAuth::fromUser($user);
        //$user['token'] = $token;


        //$user =  new UserResource($user);
//        $user->notify(new ValidationEmail($user));
        //if ($token && $user) {return $this->createdResponse($user);}
        //$this->unKnowError();
    }

    /**
     * Login with a specific user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        $rules = [
            'email'      =>'required|string:max:191',
            'password'   =>'required|string|max:191',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        if (!$token = JWTAuth::attempt(
            [
                'email'=>$request->email,
                'password'=>$request->password
            ]))
        {
            if (!$token = JWTAuth::attempt(
                [
                    'phone'=>$request->email,
                    'password'=>$request->password
                ]))
                return $this->apiResponse(null,__('messages.wrong_credentials'),406);
        }

        $user = User::where('email',$request->email)->first();

        if(!$user){
            $user = User::where('phone',$request->email)->first();
        }


//        if ($user->is_verified==0){
////            $user->verification_code=mt_rand(1000,9999);
////            $user->save();
////            $user->notify(new ValidationEmail($user));
////            $user['token'] = $token;
////            $user =  new UserResource($user);
////            return $this->apiResponse($user);
//        }
        $user['token'] = $token;
        $user =  new UserResource($user);
        if ($token && $user) {
            return $this->apiResponse($user);
        }
        $this->unKnowError();
    }


    public function getFcmToken(){
        $user = auth()->user();
        if($user){
            return \response()->json([
                'status'                => true,
                'message'               => ' fcm token ',
                'fcm_token_android'     => $user->fcm_token_android,
                'fcm_token_ios'         => $user->fcm_token_ios
            ],200);
        }else{
            return \response()->json([
                'status'                => false,
                'message'               => ' required login ',
            ],404);
        }


    }


    public function updateFcmToken(Request $request){
        $user = auth()->user();


        if($user){

            $user->fcm_token_android = $request->fcm_token_android;
            $user->fcm_token_ios = $request->fcm_token_ios;

            $user->save();


            return \response()->json([
                'status'                => true,
                'message'               => ' fcm token ',
                'fcm_token_android'     => $user->fcm_token_android,
                'fcm_token_ios'         => $user->fcm_token_ios
            ],200);
        }else{

            return \response()->json([
                'status'                => false,
                'message'               => ' required login ',
            ],404);
        }


    }


    /**
     * Forget Password
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function forget_password(Request $request)
    {
        $user=User::where('email',$request->email)->first();

        if ($user){
//            $code=GenerateMobileConfirmation();

            $verification_code = mt_rand(1000,9999);

            $user->verification_code = $verification_code;
            $user->save();

            SendEmail($user->email,$user->email,$user->verification_code);

            $user->save();

//            $msq="Khadoum Application verification code :" . $user->confirmation_code;
//            sendSms($msq,$user->phone);
            //$user->notify(new ConfirmationEmail($user));

            return \response()->json([
                'status'    => true,
                'message'   => "تم ارسال كود لاعاده كلمه السر "
            ],200);
//            $this->SendSMS($user->phone,'كود اعادة التعيين \n',$code);
            //return $this->apiResponse(__('messages.success_sent'));
        }else{

            return $this->apiResponse('',__('messages.wrong_phone'),'402');

        }
    }

    public function reset_password(Request $request)
    {  $rules = [
        'reset_code'=>'required',
        'email'=>'required',
        'password'=>'required|confirmed|min:6',
    ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response)return $validation;

        $user=User::where('email',$request['email'])
            ->where('verification_code',$request['reset_code'])->first();
        if ($user){
            $token = JWTAuth::fromUser($user);
            $user->verification_code=0;
            $user['password'] = Hash::make($request->password);
            $user->save();

            $user['token'] = $token;

            $user =  new UserResource($user);

            if ($token && $user) {return $this->apiResponse($user);}

            $this->unKnowError();

            return $this->apiResponse(__($user,null,200));
        }else{
            return $this->apiResponse('',__('messages.error_code'),'402');
        }
    }

    public function mobile_activation(Request $request)
    {
        $rules = [
            'email'            => 'required',
            'verification_code'=>'required'
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response)return $validation;

        $user=User::where('email',$request['email'])
            ->where('verification_code', $request['verification_code'])->first();
        if ($user){
            $user->is_verified=1;
            $user->verification_code=null;
            $user->save();
            $token = JWTAuth::fromUser($user);

            $user['token'] = $token;
            $user =  new UserResource($user);
            return $this->apiResponse($user);

//            return $this->apiResponse(__('messages.success_code'));

        }else{
            return $this->apiResponse('',__('messages.error_code'),'402');
        }
    }


    public function check_code(Request $request)
    {
        $rules = [
            'code'=>'required',
            'phone'=>'required',
        ];

        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response)return $validation;

        $user=User::where('phone',$request['phone'])->where('confirmation_code',$request['code'])->first();
        if ($user){
            return $this->apiResponse(__('messages.success_code'));
        }
        else{
            return $this->apiResponse('',__('messages.error_code'),'402');
        }
    }

    public function resend_code(Request $request)
    {
        $rules = [
            'email'=>'required',
        ];

        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response)return $validation;


        $user=User::where('email',$request['email'])->first();


        if ($user){

            $verification_code = mt_rand(1000,9999);
            $user->verification_code = $verification_code;
            $user->save();

            SendEmail($user->email,$user->name,$verification_code);

            return \response()->json([
                'status'    => true,
                'message'   => "تم اعاده ارسال الكود بالفعل "
            ],200);


        }

        else{
            return $this->apiResponse('',__('messages.error_code'),'402');
        }
    }
    public function Logout()
    {
        auth()->user()->update([
            'fcm_token_android'=>null,
            'fcm_token_ios'=>null,
        ]);

        return $this->apiResponse(__('تم تسجيل الخروج بنجاح بنجاح'));
    }

    /**
     * Update User Profile
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function UpdateProfile(Request $request)
    {
//        $request['role'] = $type;
        //$user = auth()->user();

        $user = JWTAuth::user();

        //dd($user);
        if(!$user){
            return \response()->json([
                'status'    => false,
                'message'   => "من فضلك قم بتسجيل الدخول "
            ],401);
        }

        //dd($user->role);
        if($user){

            $rules = [
                'name'  =>'nullable|string|max:191',
                'email' => 'nullable|string|email|max:255|unique:users,email,' . $user->id,
                'phone'      =>'nullable|string|max:191|unique:users,phone,'.$user->id,
                'image' =>'nullable|image|mimes:jpg,jpeg,gif,png',
                'address'                   =>'nullable|string',

//          'sub_category_id'       =>'required_if:role,==,provider|integer|exists:regions,id',
            ];
            $validation=$this->apiValidation($request,$rules);

            if($validation instanceof Response){return $validation;}
            if ($request->image != null)
            {
                if ($request->hasFile('image')) {
                    $picture = uploader($request,'image');
                    $user->image = $picture;
                }
            }

            $user->name = ($request->name)? $request->name : $user->name;
            $user->email = ($request->email)? $request->email : $user->email;
            $user->phone = ($request->phone)? $request->phone : $user->phone;
            $user->address = ($request->address)? $request->address : $user->address;


            if($user->save()){

                $token = JWTAuth::fromUser($user);

                $user['token'] = $token;

                $user =  new UserResource($user);

                return $this->apiResponse($user);
            }else{
                return \response()->json([
                    'status'    => false,
                    'message'   => "تأكد من البيانات"
                ],401);
            }



        }else{
                return \response()->json([
                    'status'    => false,
                    'message'   => "تأكد من البيانات"
                ],401);
            }

        }


    /**
     * Update User Profile
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */

    public function UpdateLocation(Request $request)
    {
        $user = auth()->user();
        $rules = [
            'lat' =>'required|string|max:191',
            'lng'  =>'required|string|max:191',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

      $this->UpdateClientProfile($user,$request);
        if ($user)  return $this->apiResponse(new SingleUserResource($user),null,200);
       return $this->unKnowError();
    }

    /**
     * Update User Setting
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */


    public function UpdateSetting(Request $request)
    {
        $user = auth()->user();
        $rules = [
            'image_download' =>'required|boolean',
            'language'  =>'required|string|max:191',
            'country'  =>'required|string|max:191',
            'notifiable' =>'required|boolean',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}
         $this->UpdateClientSetting($user,$request);
        if ($user)  return $this->apiResponse(new UserResource(auth()->user()),null,200);
        return $this->unKnowError();
    }


    public function GetProfile($id){
        $provider=User::find($id);
        $favourite=\App\Favourites::where('user_id',auth()->id())->where('provider_id',$id)->first();
        if($provider){
            if($provider->role=='provider'){
                $orders=$provider->comment() ;
                $provider =  new UserResource($provider);
                $like=false;
                if($favourite){
                    $like=true;
                }
                return $this->createdResponse(['rate'=>$provider->rate(),'like'=>$like,'provider'=>$provider,'comment'=>$orders]);
            }else{
               return $this->unKnowError();
            }
        }else{
            return $this->unKnowError();
        }
    }

    public function GetProvider($id){
        $provider=User::find($id);
        $provider =  new UserResource($provider);
        if($provider){
            if($provider->role=='provider'){
                return $this->createdResponse(['provider'=>$provider]);
            }else{
                return $this->unKnowError();
            }
        }else{
            return $this->unKnowError();
        }

    }

}
