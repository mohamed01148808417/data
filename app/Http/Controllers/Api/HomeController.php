<?php

namespace App\Http\Controllers\Api;

use App\Ad;
use App\AdsCategory;
use App\Category;
use App\Chat;
use App\City;
use App\Comment;
use App\Contact_Us;
use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Resources\AdsCategoryResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\chatCollection;
use App\Http\Resources\chatSingle;
use App\Http\Resources\CommentResource;
use App\Http\Resources\InboxResource;
use App\Http\Resources\MarketResource;
use App\Http\Resources\MarketSingleResource;
use App\Http\Resources\NationalityResource;
use App\Http\Resources\ProviderResource;
use App\Http\Resources\QualificationResource;
use App\Http\Resources\SingleInboxResource;
use App\Http\Resources\SlideResource;
use App\Http\Resources\UserResource;
use App\Http\Traits\ApiResponses;
use App\Inbox;
use App\Order;
use App\Qualification;
use App\Service;
use App\Setting;
use App\Slider;
use App\SubCategory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;


class HomeController extends Controller
{
    use ApiResponses;

    public function GetAllCategory()
    {
        $categories = Category::all();
        $countries = CategoryResource::collection($categories);
        return $this->apiResponse($countries);
    }

    public function getTerms()
    {
        $terms = Setting::find(1);
        return $this->apiResponse($terms);
    }

    public function getAdsCategories(){
        $ads_categories = AdsCategory::all();
        $ads_categories = AdsCategoryResource::collection($ads_categories);
        return $this->apiResponse($ads_categories);
    }

    public function getAdsProviders($ads_category_id){
        $users = User::where('ads_category',$ads_category_id)->get();

        if(count($users) == 0){
            return response()->json([
                'status'    => false,
                'message'   => ' لا يوجد مزودين خدمة حاليا لهذا القسم من الاعلانات'

            ]);
        }
        $users = UserResource::collection($users);

        return $this->apiResponse($users);
    }
    public function GetAllQualification()
    {
        $qualifications = Qualification ::all();
        $qualifications = QualificationResource::collection($qualifications);
        return $this->apiResponse($qualifications);
    }

    public function GetSubCategoryByCategory($id)
    {
        $sub_categoires = SubCategory::where('category_id',$id)->get();
        $sub_categoires = CategoryResource::collection($sub_categoires);
        return $this->apiResponse($sub_categoires);
    }

    public function GetSubCategories()
    {
        $sub_categoires = SubCategory::all();
        $sub_categoires = CategoryResource::collection($sub_categoires);
        return $this->apiResponse($sub_categoires);
    }


    public function getSlideShow(){
        $sliders = Slider::all();
        $sliders = SlideResource::collection($sliders);
        return $this->apiResponse($sliders);

    }

    public function GetServicesByCategory($id)
    {
        $services = Service::where('sub_category_id',$id)->get();
        $services = CategoryResource::collection($services);
        return $this->apiResponse($services);
    }

    public function GetProviderBySubCategory($id)
    {
        $sub_categoires = SubCategory::findOrFail($id);
        $providers= $sub_categoires->PUsers();
        return $this->apiResponse(new ProviderResource($providers));
    }

    public function GetProviderByAdsCategory($id)
    {
        $sub_categoires = AdsCategory::findOrFail($id);
        $providers= $sub_categoires->PUsers();
        return $this->apiResponse(new ProviderResource($providers));
    }

    public function GetServicesProviders($id,Request $request)
    {

        $rules = [
            'lat' =>'required|numeric',
            'lng'  =>'required|numeric',
        ];

        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        $providers = User::where('role','provider')->select('*')
            ->selectRaw('( 3959 * acos( cos( radians(?) ) *
                                  cos( radians( `lat` ) )
                                  * cos( radians( `lng` ) - radians(?)
                                  ) + sin( radians(?) ) *
                                  sin( radians( `lat` ) ) )
                                ) AS distance',
                [$request->lat, $request->lng, $request->lat])
            ->havingRaw("distance < ?", ['50'])
            ->simplePaginate();


        return $this->apiResponse(new ProviderResource($providers));
    }

    public function GetNearestProviders(Request $request)
    {

        $rules = [
            'lat' =>'required|numeric',
            'lng'  =>'required|numeric',
        ];

        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        $providers = User::where('role','provider')->select('*')->selectRaw('( 3959 * acos( cos( radians(?) ) *
                                  cos( radians( `lat` ) )
                                  * cos( radians( `lng` ) - radians(?)
                                  ) + sin( radians(?) ) *
                                  sin( radians( `lat` ) ) )
                                ) AS distance', [$request->lat, $request->lng, $request->lat])
            ->havingRaw("distance < ?", ['50'])
            ->simplePaginate();


        return $this->apiResponse(new ProviderResource($providers));
    }

    public function marketRequest() {
        $ads=Ad::where('type','order')->paginate();
        return $this->apiResponse(new MarketResource($ads));
     }

    public function marketOffers() {
        $ads=Ad::where('type','offer')->paginate();
        return $this->apiResponse(new MarketResource($ads));
    }

    public function marketSingle($id) {
        $ads=Ad::findOrFail($id);
        return $this->apiResponse(new MarketSingleResource($ads));
    }

    public function MarketAddRequest(Request $request){

        $rules = [
            'title' => 'required|string|',

            'description' => 'required|string|',
            'image' => 'required|image',
            'attachment' => 'required|file',
            'time_count' => 'required|string',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}
        $inputs=$request->all();
        $inputs['user_id']=auth()->id();
        $inputs['type']='order';
        $image = uploader($request,'image');
        $inputs['image']=$image;
        $attachment= uploader($request,'attachment');
        $inputs['attachment']=$attachment;
        $ad=Ad::create($inputs);
        return $this->apiResponse(new MarketSingleResource($ad));

    }
    public function MarketAddOffer(Request $request){

        $rules = [
            'title' => 'required|string|',

            'description' => 'required|string|',
            'image' => 'required|image',
            'attachment' => 'required|file',
            'time_count' => 'required|string',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}
        $inputs=$request->all();
        $inputs['user_id']=auth()->id();
        $inputs['type']='offer';
        $image = uploader($request,'image');
        $inputs['image']=$image;
        $attachment= uploader($request,'attachment');
        $inputs['attachment']=$attachment;
        $ad=Ad::create($inputs);
        return $this->apiResponse(new MarketSingleResource($ad));

    }
    public function MarketAddComment($id,Request $request){
        $ads=Ad::findOrFail($id);
        $rules = [
            'comment' => 'required|string|',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        $comment=new Comment();
        $comment->ads_id=$ads->id;
        $comment->user_id=auth()->id();
        $comment->comment=$request->comment;
        $comment->save();

        return $this->apiResponse(new CommentResource($comment));;

    }

    public function about()
    {
        return $this->apiResponse(getsetting('about'));
    }

    public function GetRandomAds(){
        $ads=Ad::take(3)->get();

        foreach ($ads as $ad){
            $ad['image']=getimg($ad['image']);
        }

        return $this->createdResponse($ads);
    }


    public function terms()
    {
        return $this->apiResponse(getsetting('terms'));
    }

    public function terms_user()
    {
        return $this->apiResponse(getsetting('terms_user'));
    }

    public function terms_provider()
    {
        return $this->apiResponse(getsetting('terms_provider'));
    }

    public function search(Request $request){

        $rules = [
            'keyword' =>'nullable|string',
            'sub_category_id'  =>'nullable|string|max:191',
            'country_id'  =>'nullable|string|max:191',
            'city_id'  =>'nullable|string|max:191',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        //        if($request->has('city'))
        //        {
        //            if($request->has('region')){
        //                if ($request->has('rate')){
        //                    $providers = User::where('role', 'provider')->Where('name', 'like', '%' . $request['keyword'] . '%')->where('region_id', $request->region)
        //                        ->with('orders3')->get()->transform(function ($q){
        //                            return ['rate'=>fix_null($q->orders3()->groupBy('provider_id')->avg('rate')),'user_id'=>$q->id];
        //                        })->where('rate','>=',$request['rate'])->pluck('user_id');
        //
        //                    $providers= User::whereIn('id',$providers)->paginate(15);
        //                }else{
        //                    $providers = User::where('role', 'provider')->Where('name', 'like', '%' . $request['keyword'] . '%')->where('region_id',$request->region)->paginate(15);
        //                }
        //            }
        //            elseif ($request->has('rate')){
        //                $regions = \App\Region::where('city_id',$request->city)->pluck('id');
        //                $providers = User::where('role', 'provider')->Where('name', 'like', '%' . $request['keyword'] . '%')->whereIn('region_id', $regions)
        //                    ->with('orders3')->get()->transform(function ($q){
        //                        return ['rate'=>fix_null($q->orders3()->groupBy('provider_id')->avg('rate')),'user_id'=>$q->id];
        //                    })->where('rate','>=',$request['rate'])->pluck('user_id');
        //                $providers= User::whereIn('id',$providers)->paginate(15);
        //            }
        //            else {
        //                $regions = \App\Region::where('city_id',$request->city)->pluck('id');
        //                $providers = User::where('role', 'provider')->Where('name', 'like', '%' . $request['keyword'] . '%')->whereIn('region_id', $regions)->paginate(15);
        //            }
        //        }
        //        else{
        //            if($request->has('rate')){
        //
        //                $providers = User::where('role', 'provider')->Where('name', 'like', '%' . $request['keyword'] . '%')->where('service_id', $request->service)
        //                    ->with('orders3')->get()->transform(function ($q){
        //                        return ['rate'=>fix_null($q->orders3()->groupBy('provider_id')->avg('rate')),'user_id'=>$q->id];
        //                })->where('rate','>=',$request['rate'])->pluck('user_id');
        //
        //                $providers= User::whereIn('id',$providers)->paginate(15);
        //            }
        //            else {
        //                if($request->has('lat') && $request->has('lng'))
        //                {
        //                    $providers = User::where('role', 'provider')->Where('name', 'like', '%' . $request['keyword'] . '%')->where('service_id', $request->service)->select('*')->selectRaw('( 3959 * acos( cos( radians(?) ) *
        //                                  cos( radians( `lat` ) )
        //                                  * cos( radians( `lng` ) - radians(?)
        //                                  ) + sin( radians(?) ) *
        //                                  sin( radians( `lat` ) ) )
        //                                ) AS distance', [$request->lat, $request->lng, $request->lat])
        //                        ->havingRaw("distance < ?", ['20'])
        //                        ->simplePaginate();
        //                }
        //                else {
        //                    $providers = User::where('role', 'provider')->Where('name', 'like', '%' . $request['keyword'] . '%')->where('service_id', $request->service)->paginate(15);
        //                }
        //            }
        //        }

        $query = User::query();
        $query->where('role','provider')
            ->Where('name', 'like', '%' . $request['keyword'] . '%');

        $query->when(isset($request['sub_category_id']),function ($q) use ($request) {
            if($request['sub_category_id']!=''){
                $users=SubCategory::findOrFail($request['sub_category_id'])->UsersIds();
                return $q->whereIn('id',  $users);
            }
        });

        $query->when(isset($request['city_id']),function ($q) use ($request) {
            if($request['city_id']!='')
            {
                $users = City::findOrFail($request['city_id'])->usersId();

                return $q->whereIn('id',  $users);
            }
        });

        $query->when(isset($request['country_id']),function ($q) use ($request) {
            if($request['country_id']!='')
            {
                if(isset($request['city_id'])){
                    return $q;
                }
                else{
                    $cities=Country::findOrFail($request['country_id'])->cities;
                    $users=[];

                    foreach ($cities as $city){
                        if($city->usersId()){
                            foreach ($city->usersId() as $item){
                                array_push($users,$item);
                            }


                        }
                    }


                    $users=array_unique($users);
                    return $q->whereIn('id',  $users);

                }}
        });

        $providers=$query->paginate();
        return $this->apiResponse(new ProviderResource($providers));

    }

    public  function  contact (Request $request){
        $rules = [
            'name' =>'required|string|max:191',
            'email'  =>'required|email|max:191',
            'message'  =>'required|string|max:791',
        ];

        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

            $contacts= new Contact_Us();
            $contacts->user_id=auth()->id();
            $contacts->email=$request->email;
            $contacts->name=$request->name;
            $contacts->message=$request->message;
            $contacts->save();

        return $this->createdResponse($contacts);

    }

    public function addToFavourites(Request $request){
        $rules = [
            'provider_id' =>'required|string|exists:users,id',
        ];

        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        $favourites=  \App\Favourites::where('user_id',auth()->id())->where('provider_id',$request->provider_id)->first();

        if($favourites){
            return $this->apiResponse(__('موجود فى المفضلة'));
        }
        $favourites= new \App\Favourites();
        $favourites->provider_id=$request->provider_id;
        $favourites->user_id=auth()->id();
        $favourites->save();
        return $this->apiResponse(__('تم الاضافة إلى المفضلة'));
    }

    public function removeFromFavourites(Request $request){
        $rules = [
            'provider_id' =>'required|string|exists:users,id',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}
        $favourites=  \App\Favourites::where('user_id',auth()->id())->where('provider_id',$request->provider_id)->first();

        if($favourites){
            $favourites->delete();
            return $this->apiResponse(__('تم الحذف إلى المفضلة'));
        }
        return $this->apiResponse(__(' غير موجود بالمفضلة '));
    }

    public  function Favourites(){
        $favourites=  auth()->user()->favourites_user->transform(function ($q){return $q->providers;})->values();
//        return \response()->json($favourites);
        return $this->apiResponse(new ProviderResource($this->CollectionPaginate($favourites)));

    }


    public function  send(Request $request){
        $rules = [
            'message' =>'required|string',
            'order_id' =>'required',
            'receiver_id' =>'required|string',
        ];



        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        $id=auth()->id();
        $request['sender_id']=$id;
        $request['user_id']=$request->receiver_id;

        $order = Order::find($request['order_id']);
        $receiver = User::find($request['receiver_id']);

        if($order && $receiver){
            $inbox = new Inbox();
            $inbox->order_id = $order->id;
            $inbox->user_id = $receiver->id;
            $inbox->sender_id = $id;
            $inbox->message = $request['message'];
            $inbox->created_at = Carbon::now();

            $inbox->save();
            $this->notifyByFirebase(
                'لديكم رسالة جديدة',
                'تطبيق شعارى',
                [$receiver->fcm_token_android],
                ['type'=>'message',
                    'data'=>['order_id'=>$request['order_id'] ,
                        'sender_id'=> $inbox->sender_id ,
                        'user_name'=>$inbox->sender->name ]]);


            $chats = Inbox::where('order_id',$order->id)->orderBy('id','ASC')->paginate(10);
            if($chats){
                $chats = new chatCollection($chats);
            }



            return \response()->json([
                'status'    => true,
                'msg'       => ' تم ارسال الرساله',
                'data'      => (new chatSingle($inbox))
            ],200);
        }else{
            return \response()->json([
                'status'    => false,
                'msg'       => 'تأكد من البيانات'
            ],200);
        }
    }

    public function showMessage($order_id){
        $auth = auth()->user();
        $order=Order::find($order_id);

        if($order){
            if($order->user_id == $auth->id || $order->provider_id == $auth->id){
                $chats = Inbox::where('order_id',$order->id)->orderBy('id','ASC')->paginate(10);
                if($chats){
                    $chats = new chatCollection($chats);

                    return $this->apiResponse($chats);
                }else{
                    return response()->json([
                        'status'    => true,
                        'msg'       => ' chat is empty'
                    ],200);
                }
            }else{
                return $this->apiResponse(null,'you are not authorized to get this chat details',403);
            }
        }

    }
    public function inbox(){
        $id=auth()->id();
        $inbox= Inbox::where('sender_id',$id)
            ->orWhere('user_id',$id)->latest()->with('User')
            ->with('Sender')->paginate(15);
        return $this->apiResponse( new InboxResource($inbox));
    }

    public function nationalities(){
        $nationalities=\App\Nationality::all();
        $nationalities = NationalityResource::collection($nationalities);

        return $this->apiResponse($nationalities);
    }

    public function notifyByFirebase($title, $body, $tokens, $data = [] , $is_notification = true)
    {
        // https://gist.github.com/rolinger/d6500d65128db95f004041c2b636753a
        // API access key from Google FCM App Console
        // env('FCM_API_ACCESS_KEY'));

        //    $singleID = 'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd';
        //    $registrationIDs = array(
        //        'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd',
        //        'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd',
        //        'eEvFbrtfRMA:APA91bFoT2XFPeM5bLQdsa8-HpVbOIllzgITD8gL9wohZBg9U.............mNYTUewd8pjBtoywd'
        //    );
        $registrationIDs = $tokens;

        // prep the bundle
        // to see all the options for FCM to/notification payload:
        // https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support

        // 'vibrate' available in GCM, but not in FCM
        $fcmMsg = array(
            'body' => $body,
            'title' => $title,
            'sound' => "default",
            'color' => "#203E78"
        );
        // I haven't figured 'color' out yet.
        // On one phone 'color' was the background color behind the actual app icon.  (ie Samsung Galaxy S5)
        // On another phone, it was the color of the app icon. (ie: LG K20 Plush)

        // 'to' => $singleID ;      // expecting a single ID
        // 'registration_ids' => $registrationIDs ;     // expects an array of ids
        // 'priority' => 'high' ; // options are normal and high, if not set, defaults to high.
        $fcmFields = array(
            'registration_ids' => $registrationIDs,
            'priority' => 'high',
            'data' => $data
        );
        if ($is_notification)
        {
            $fcmFields['notification'] = $fcmMsg;
        }

        $headers = array(
            'Authorization: key=AAAANLaLRLM:APA91bHzUSCCZ5zaryOCg_6Jh7a5S61LQbqFzEVE4alKrDuqgcIWqMAw_dTxOO6Wo8n_uXhoOOobzvIurArwievayfEWGUW0m-z_xqi0wUtmod2Rgvwm3z9afrfOxMwtX_stZMhvSwwV',
            'Content-Type: application/json'
        );

        /*        info("API_ACCESS_KEY_client: ".env('API_ACCESS_KEY_client'));
               info("PUSHER_APP_ID: ".env('PUSHER_APP_ID'));*/

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function checkEmail(Request $request){

        $user = User::where('email',$request->email)->first();

        if($user){
            return \response()->json([
                'status'    => false,
                'msg'       => 'Email already exist'
            ],200);
        }else{

            return \response()->json([
                'status'    => true,
                'msg'       => 'Email Available'
            ],200);
        }
    }

    public function checkPhone(Request $request){

        $user = User::where('phone',$request->phone)->first();

        if($user){
            return \response()->json([
                'status'    => false,
                'msg'       => 'Phone already exist'
            ],200);
        }else{

            return \response()->json([
                'status'    => true,
                'msg'       => 'Phone Available'
            ],200);
        }
    }

}
