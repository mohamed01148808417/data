<?php

namespace App\Http\Traits;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;


trait Paypalable
{

    /**
     * Paypal Configuration
     * @var
     */
    protected $apiContext;
    protected $accounts = [
        'client_id' => 'AVbBsUHaH3yQBmppaxe5cofS7lPNKRYt1Egvg-aX6ZJqCTxML2cMRDWYNnmVK2ZgNNim7G155ajJeV0x',
        'secret_client' => 'EOsep9-6NjkqncXyx_NEzRQ0eSzN2Qxal4zPPR3EtxvIJsh4T02p73s0LRni57PL3MZ7syXq1iqUyZ-J',
    ];
    protected $settings = ['mode' => 'sandbox', 'http.ConnectionTimeOut' => 30,
        'log.logEnable' => true, 'logFileName' => '/logs/paypal.log'];


    /**
     * Configure Client_id,Secret_client
     * Context
     */
    public function setApiContext(){

        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->accounts['client_id'],
                $this->accounts['secret_client']
            )
        );

        $this->apiContext->setConfig($this->settings);

    }



}