<?php

namespace App\Http\Controllers\Auth;

use App\SubCategory;
use App\User;
use App\Http\Controllers\Controller;
use App\UserSubCategory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/active/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [

            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:2|confirmed',
            'city_id' => 'required',
            'phone' => 'required|string|min:9|unique:users',
            'image' => 'nullable',
            'agree' => 'required',
            ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create( array $data)
    {



        if (isset($data['provider_type'])){
            $data['role']='provider';
            $request = request();
            //dd($request->all());
               if($request->hasfile('image'))
               {
                   $img= uploader( $request , 'image');
//                 dd($request['image']);
                   $data['image']= $img;
//                 dd(2);
               }

            if($data['provider_company_type']){
                   if($data['provider_company_type']=='LTD' || $data['provider_company_type']=='MNC' ){
                       $data['is_special']=1;
//                       dd($data);
                   }
            }

            $data['password']=Hash::make($data['password']);
            $data['verification_code'] = mt_rand(1000,9999);
            $user=User::create($data);

            $user->save();

            if($data['sub_catigories'] != null)
            {

                foreach ($data['sub_catigories'] as $sub)
                {
                    $usersub=new UserSubCategory();
                    $usersub->user_id=$user->id;
                    $usersub->sub_category_id=$sub;
                    $usersub->save();
                }
            }

            $numbers = $user->phone;						   	//the mobile number or set of mobiles numbers that the SMS message will be sent to them, each number must be in international format, without zeros or symbol (+), and separated from others by the symbol (,).
            $msg = " كود التفعيل   ".$user->verification_code;
            $MsgID = rand(1,99999);
            $result=sendSMS('966506601144',
                'Sheari1396',
                $numbers,
                'sheari',
                $msg, $MsgID,
                0,
                0,
                '152485',
                1);


            // send card to user mail
            $htmlStr = "";
            $htmlStr .= "Hi " . $user->name . ",<br /><br />";

            $htmlStr .= "SHEARI HOME PAGE  <br /><br /><br />";
            $htmlStr .= "<a href=".url('/')." target='_blank' style='padding:1em; font-weight:bold; background-color:blue; color:#fff;'>SHEARI HOME PAGE </a><br /><br /><br />";

            $htmlStr .= "
            
            
            <div dir='rtl'
             class='card' style='width: 600px; background-color: #015b93; min-height: 600px; margin: auto; padding-bottom: 40px;'>
            <div class top=''>
                <div style='width: 33%; text-align: center; display: inline-block;'>
                    <p style='font-size:16px; color: #fff; text-align: center;'>
                        يتمتع صاحب هذه العضوية 
                        <br>
                        بميزات الموقع الممنوحة
                        <br>
                        وفق ميثاق التعاون الرقمي
                    </p>

                </div>

                <div style='width: 33%; text-align: center; display: inline-block;'>
                    <img width='100' src='sheari-logo-3.png' style='width:100px;  height: 100px; margin: 0;'/>
                </div>

                <div style='width: 32%; text-align: center; display: inline-block; line-height: 5px;'>
                    <p style='color: #fff; font-size: 33px;'>عضوية</p>
                    <p style='color: #fff; font-size: 15px;'>الفنون الرقمية بمنطقة شعاري</p>
                    <p style='color: #fff; font-size: 16px;'><a style='color: #fff;' href='http://www.sheari.net'>www.sheari.net</a></p>
                </div>
            </div>


            <div class='details' style='width: 100%; margin-top: 30px;'>
                <div style='width: 40%; display: inline-block'>
                    
                    <p style='text-align: center; color:#FFF;
                    background-color:#047;font-size: 30px;margin: 35px;padding-bottom: 11px;border-radius: 16px;'>العضو</p></div>
                <div style='width: 59%; display: inline-block;'><p
                    

                    style='color: #fff; text-align: center; font-size: 25px; border: 1px solid #fff;width: 236px; border-radius: 15px;padding: 10px 46px; padding-bottom: 17px;'

                    > $user->name</p></div> 
            </div>


            <div class='details' style='width: 100%; margin-top: 30px;'>
                <div style='width: 40%; display: inline-block'>
                    
                    <p style='text-align: center; color:#FFF;
                    background-color:#2c2c2c;font-size: 30px;margin: 35px;padding-bottom: 11px;border-radius: 16px;'>المجال</p></div>
                <div style='width: 59%; display: inline-block;'><p
                    

                    style='color: #fff; font-size: 25px; border: 1px solid #fff; width: 236px; text-align: center; border-radius: 15px;padding: 10px 46px; padding-bottom: 17px;'> تفاصيل المجال الرئيسي </p>
                </div>

                <div style='width:100%'><span style='
                display: block;margin-top: -18px;text-align: center;color:#FFF;font-size: 12px;margin-right: 207px;
                '> موقع وتطبيق شعاري قيم خدماتنا عن طريق الموقع والتطبيق  </span></div>
                
                

            </div>


            <div class='details' style='width: 100%; margin-top: 30px;'>
                <div style='width: 40%; display: inline-block'>
                    
                    <p style='text-align: center; color:#FFF;
                    background-color:#047;font-size: 30px;margin: 35px;padding-bottom: 11px;border-radius: 16px;'>البوابة</p></div>
                <div style='width: 59%; display: inline-block;'><p
                    

                    style='color: #fff; font-size: 25px; border: 1px solid #fff;  width: 236px; text-align: center; border-radius: 15px;padding: 10px 46px; padding-bottom: 17px;'

                    >  ---  </p></div> 
            </div>



            <div class='details' style='width: 100%; margin-top: 30px;'>
                <div style='width: 40%; display: inline-block'>
                    
                    <p style='text-align: center; color:#FFF;
                    background-color:#2c2c2c;font-size: 30px;margin: 35px;padding-bottom: 11px;border-radius: 16px;'>التواصل</p></div>
                <div style='width: 59%; display: inline-block;'><p
                    

                    style='color: #fff; font-size: 15px; border: 1px solid #fff; width: 236px; text-align: center; border-radius: 15px;padding: 15px 46px; padding-bottom: 17px;'> تواصل معنا عن طريق التطبيق    </p>
                </div>

                <div style='width:100%'><span style='
                display: block;margin-top: -18px;text-align: center;color:#FFF;font-size: 12px;margin-right: 207px;
                '> موقع وتطبيق شعاري قيم خدماتنا عن طريق الموقع والتطبيق  </span></div>
                
            </div>

        </div>
            
            
            ";


            $htmlStr .= "Kind regards,<br />";
            $name = "mohamed";
            $email_sender = "mohamed01148808417@gmail.com";
            // password mail Mohamed01148808417@

            $subject = "YOUR CARD ";
            $recipient_email = $user->email;

            $headers  = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=utf-8";
            $headers .= "From: {$name} <{$email_sender}> \n";
            $body = $htmlStr;

            mail($recipient_email, $subject, $body, $headers);

            /*end card */

            return $user;
        }

        else{
            $user= User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'role' =>'client',
                'city_id' => $data['city_id'],
                'password' => Hash::make($data['password']),
            ]);

            $user->verification_code=mt_rand(1000,9999);
            $user->save();
            $numbers = $user->phone;						   	//the mobile number or set of mobiles numbers that the SMS message will be sent to them, each number must be in international format, without zeros or symbol (+), and separated from others by the symbol (,).
            $msg = " كود التفعيل   ".$user->verification_code;
            $MsgID = rand(1,99999);
            $result=sendSMS('966506601144',
                'Sheari1396', $numbers, 'sheari', $msg, $MsgID, 0, 0, '152485',1);

            /* send card to user mail */

            // send card to user mail
            $htmlStr = "";
            $htmlStr .= "Hi " . $user->name . ",<br /><br />";

            $htmlStr .= "SHEARI HOME PAGE  <br /><br /><br />";
            $htmlStr .= "<a href=".url('/')." target='_blank' style='padding:1em; font-weight:bold; background-color:blue; color:#fff;'>SHEARI HOME PAGE </a><br /><br /><br />";

            $htmlStr .= "
            
            
            <div dir='rtl'
             class='card' style='width: 600px; background-color: #015b93; min-height: 600px; margin: auto; padding-bottom: 40px;'>
            <div class top=''>
                <div style='width: 33%; text-align: center; display: inline-block;'>
                    <p style='font-size:16px; color: #fff; text-align: center;'>
                        يتمتع صاحب هذه العضوية 
                        <br>
                        بميزات الموقع الممنوحة
                        <br>
                        وفق ميثاق التعاون الرقمي
                    </p>

                </div>

                <div style='width: 33%; text-align: center; display: inline-block;'>
                    <img width='100' src='sheari-logo-3.png' style='width:100px;  height: 100px; margin: 0;'/>
                </div>

                <div style='width: 32%; text-align: center; display: inline-block; line-height: 5px;'>
                    <p style='color: #fff; font-size: 33px;'>عضوية</p>
                    <p style='color: #fff; font-size: 15px;'>الفنون الرقمية بمنطقة شعاري</p>
                    <p style='color: #fff; font-size: 16px;'><a style='color: #fff;' href='http://www.sheari.net'>www.sheari.net</a></p>
                </div>
            </div>


            <div class='details' style='width: 100%; margin-top: 30px;'>
                <div style='width: 40%; display: inline-block'>
                    
                    <p style='text-align: center; color:#FFF;
                    background-color:#047;font-size: 30px;margin: 35px;padding-bottom: 11px;border-radius: 16px;'>العضو</p></div>
                <div style='width: 59%; display: inline-block;'><p
                    

                    style='color: #fff; text-align: center; font-size: 25px; border: 1px solid #fff;width: 236px; border-radius: 15px;padding: 10px 46px; padding-bottom: 17px;'

                    > $user->name</p></div> 
            </div>


            <div class='details' style='width: 100%; margin-top: 30px;'>
                <div style='width: 40%; display: inline-block'>
                    
                    <p style='text-align: center; color:#FFF;
                    background-color:#2c2c2c;font-size: 30px;margin: 35px;padding-bottom: 11px;border-radius: 16px;'>المجال</p></div>
                <div style='width: 59%; display: inline-block;'><p
                    

                    style='color: #fff; font-size: 25px; border: 1px solid #fff; width: 236px; text-align: center; border-radius: 15px;padding: 10px 46px; padding-bottom: 17px;'> تفاصيل المجال الرئيسي </p>
                </div>

                <div style='width:100%'><span style='
                display: block;margin-top: -18px;text-align: center;color:#FFF;font-size: 12px;margin-right: 207px;
                '> موقع وتطبيق شعاري قيم خدماتنا عن طريق الموقع والتطبيق  </span></div>
                
                

            </div>


            <div class='details' style='width: 100%; margin-top: 30px;'>
                <div style='width: 40%; display: inline-block'>
                    
                    <p style='text-align: center; color:#FFF;
                    background-color:#047;font-size: 30px;margin: 35px;padding-bottom: 11px;border-radius: 16px;'>البوابة</p></div>
                <div style='width: 59%; display: inline-block;'><p
                    

                    style='color: #fff; font-size: 25px; border: 1px solid #fff;  width: 236px; text-align: center; border-radius: 15px;padding: 10px 46px; padding-bottom: 17px;'

                    >  ---  </p></div> 
            </div>



            <div class='details' style='width: 100%; margin-top: 30px;'>
                <div style='width: 40%; display: inline-block'>
                    
                    <p style='text-align: center; color:#FFF;
                    background-color:#2c2c2c;font-size: 30px;margin: 35px;padding-bottom: 11px;border-radius: 16px;'>التواصل</p></div>
                <div style='width: 59%; display: inline-block;'><p
                    

                    style='color: #fff; font-size: 15px; border: 1px solid #fff; width: 236px; text-align: center; border-radius: 15px;padding: 15px 46px; padding-bottom: 17px;'> تواصل معنا عن طريق التطبيق    </p>
                </div>

                <div style='width:100%'><span style='
                display: block;margin-top: -18px;text-align: center;color:#FFF;font-size: 12px;margin-right: 207px;
                '> موقع وتطبيق شعاري قيم خدماتنا عن طريق الموقع والتطبيق  </span></div>
                
            </div>

        </div>
            
            
            ";


            $htmlStr .= "Kind regards,<br />";
            $name = "mohamed";
            $email_sender = "mohamed01148808417@gmail.com";
            // password mail Mohamed01148808417@

            $subject = "YOUR CARD ";
            $recipient_email = $user->email;

            $headers  = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=utf-8";
            $headers .= "From: {$name} <{$email_sender}> \n";
            $body = $htmlStr;

            mail($recipient_email, $subject, $body, $headers);

            /*end card */



            return  $user;
        }
    }
}
