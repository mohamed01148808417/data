<?php


namespace App\Http\Traits;


use App\Address;
use App\User;
use Hash;
use Illuminate\Http\Request;

trait UserOperation
{
   public function RegisterUser($request)
  {
      $inputs = $request->all();

      if ($request->image != null)
      {
          if ($request->hasFile('image')) {
              $picture = uploader($request,'image');
              $inputs['image'] = $picture;
          }
      }

      $inputs['password']=Hash::make($request->password);
      $verification_code = mt_rand(1000,9999);
      $inputs['verification_code'] = $verification_code;

      SendEmail($request->email,$request->name,$verification_code);

      return User::create($inputs);
  }

    public function UpdateClientProfile($user,$request)
    {
        $inputs = $request->all();
        if ($request->image != null)
        {
            if ($request->hasFile('image')) {
                $picture = uploader($request,'image');
                $user->update(['image' => $picture]);
            }
        }


        if($request->password != null) {$user->update(['password'=>Hash::make($request->password)]);}
        return $user->update(array_except($inputs,['password','image']));
    }

}