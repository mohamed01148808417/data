<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'id',
        'order_type',
        'has_files',
        'user_id',
        'pay',
        'file_payed',
        'status',
        'execution_time',
        'amount',
        'created_at',
        'updated_at'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function consultation(){
        return $this->hasOne(Consultation::class,'order_id','id');
    }

    public function contract(){
        return $this->hasOne(Contract::class,'order_id','id');
    }

    public function special(){
        return $this->hasOne(Special::class,'order_id','id');
    }

    public function images(){
        return $this->hasMany(OrderImage::class,'order_id','id');
    }

}
